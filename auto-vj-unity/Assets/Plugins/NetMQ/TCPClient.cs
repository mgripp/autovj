﻿using UnityEngine;

public class TCPClient : MonoBehaviour
{
    public TCPRequester midiRequester;
    public TCPRequester lightsRequester;

    public void Init(string midiServer, string lightsServer)
    {
        midiRequester = new TCPRequester(midiServer);
        lightsRequester = new TCPRequester(lightsServer);
        midiRequester.Start();
        lightsRequester.Start();
    }

    private void OnDestroy()
    {
        midiRequester.Stop();
        lightsRequester.Stop();
    }
}