﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraManager : MonoBehaviour
{
    static public CameraManager Instance;

    [SerializeField] Camera uiCamera;
    [SerializeField] Camera videoCamera;

    public Camera UiCamera => uiCamera;
    public Camera VideoCamera => videoCamera;

    private void Awake() => Instance = Instance ?? this;
}
