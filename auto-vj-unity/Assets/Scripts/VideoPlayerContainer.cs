// using System.Collections;
// using System.Collections.Generic;
// using UnityEngine;
// using UnityEngine.Video;

// public class VideoPlayerContainer : MonoBehaviour {
// 	// 0: aktiver Player, 1: vorwärtslaufend, 2: rückwärtslaufend
// 	VideoPlayer[] videoPlayer;	// aktiver Mediaplayer, der auf dem Screen zu sehen ist
// 	VideoPlayer[] videoPlayerAlpha;	// Player, der durchsichtig ist, um als Ebene über dem aktiven zu laufen
// 	int shakeSample = 1;			// sample, dessen Daten für Kameraverwacklungsstärke genutzt werden

// 	static float videoEndBuffer = 10; // Zeit in s, die am Ende eines Clips ausgelassen werden, wenn zufällig in ihm eine Zeit angespielt wird

// 	static float[] randomPlaybackSpeedBounds = {1f, 10f};
// 	public Renderer mainLayer;
// 	public Renderer alphaLayer;
// 	// public Shake mainShake;
// 	// public Shake alphaShake;
// 	bool mainShaking = false;
// 	bool alphaShaking = false;
// 	public bool mainReverse = false;
// 	bool alphaReverse = false;
// 	bool active = false;
// 	public bool alphaActive = false;
// 	bool reverseInProgress = false;

// 	bool ppVignetteEnabled = false;
// 	bool ppVignetteStrobo = false;

// 	bool ppGrainEnabled = false;
// 	bool ppGrainStrobo = false;
// 	bool ppGrainColored = false;
// 	float ppGrainSize = 1f;

// 	bool ppColorGradingEnabled = false;
// 	int ppColorGradingMode = 0;
// 	bool ppColorGradingHardcore = false;
// 	int ppColorGradingRedSample = 0;
// 	int ppColorGradingGreenSample = 0;
// 	int ppColorGradingBlueSample = 0;
// 	float ppTime = 0;
// 	float ppInterval = 2f;
// 	bool isFading = false;

// 	// Use this for initialization
// 	void Start () {
// 		// Arrays initialisieren
// 		videoPlayer = new VideoPlayer[3];
// 		videoPlayerAlpha = new VideoPlayer[3];

// 		// Main-Player füllen
// 		videoPlayer[1] = CreateVideoPlayer (null, null);	// Player einsetzen für vorwärtslaufend

// 		VideoClip c;

// 		// Player einsetzen für rückwärtslaufend. Dessen Clip muss 
// 		// if (Player.instance.clipReverseDict.TryGetValue(videoPlayer[1].clip, out c)) {
// 		// 	videoPlayer[2] = CreateVideoPlayer (null, c);
// 		// 	videoPlayer [2].targetMaterialRenderer = null;
// 		// }	
// 		videoPlayer[0] = videoPlayer[1];

// 		// Alpha-Player füllen
// 		videoPlayerAlpha[1] = CreateVideoPlayer (null, null);

// 		// if (Player.instance.clipReverseDict.TryGetValue(videoPlayerAlpha[1].clip, out c)) {
// 		// 	videoPlayerAlpha [2] = CreateVideoPlayer (null, c);
// 		// 	videoPlayerAlpha [2].targetMaterialRenderer = null;
// 		// }	
// 		videoPlayerAlpha[0] = videoPlayerAlpha[1];
// 	}

// 	public VideoPlayer GetActiveVideoPlayer() {
// 		return videoPlayer [0];
// 	}

// 	// public void MakeFadeLayer() {
// 	// 	mainLayer = Player.instance.fadePlaneMain.GetComponent<Renderer>();
// 	// 	alphaLayer = Player.instance.fadePlaneAlpha.GetComponent<Renderer>();
// 	// 	mainShake = Player.instance.fadePlaneMain.GetComponent<Shake>();
// 	// 	alphaShake = Player.instance.fadePlaneAlpha.GetComponent<Shake> ();
// 	// 	isFading = false;
// 	// }

// 	// public void MakeCurrentlyPlayed() {
// 	// 	mainLayer = Player.instance.planeMain.GetComponent<Renderer>();
// 	// 	alphaLayer = Player.instance.planeAlpha.GetComponent<Renderer>();
// 	// 	mainShake = Player.instance.planeMain.GetComponent<Shake>();
// 	// 	alphaShake = Player.instance.planeAlpha.GetComponent<Shake> ();
// 	// 	isFading = true;
// 	// }

// 	public void randomize() {
// 		randomPP ();
// 		if (randomBool(0.2f)) ToggleAlpha ();
// 		mainShaking = randomBool (0.6f);
// 		alphaShaking = randomBool (0.3f);
// 		ChangeClipRandom ();
// 		JumpToRandomTime (0, 0);
// 	}

// 	public void randomPP() {
// 		ppVignetteEnabled = randomBool (0.33f);
// 		ppVignetteStrobo = randomBool (0.75f);

// 		ppGrainEnabled = randomBool (0.5f);
// 		ppGrainSize = Random.Range (0.3f, 3f);
// 		ppGrainColored = randomBool (0.5f);
// 		ppGrainStrobo = randomBool (0.33f);

// 		ppColorGradingEnabled = randomBool(0.4f);
// 		//ppColorGradingEnabled = true;
// 		//ppColorGradingMode = (int)((ppColorGradingMode + 1) % 11);
// 		ppColorGradingMode = (int) Random.Range (0, 11f);
// 		if(ppColorGradingMode < 4 && randomBool(0.75f)) ppColorGradingMode = (int) Random.Range (4, 11f);	// Die ersten 4 Modi sind sehr ähnlich, so wird ein zu häufiges vorkommen verhindert
// 		ppColorGradingHardcore = false;//randomBool(0.1f);

// 		//print ("Mode: " + ppColorGradingMode);

// 		// if (ppColorGradingEnabled) {
// 		// 	cs.basic.postExposure = 5f;
// 		// 	if (ppColorGradingMode == 2 || ppColorGradingMode == 3) { 
// 		// 		cs.basic.postExposure = 6f;
// 		// 	} else if (ppColorGradingMode == 4) {
// 		// 		cs.basic.postExposure = 8f;
// 		// 	} else if (ppColorGradingMode == 8) {
// 		// 		cs.basic.postExposure = 2.5f;
// 		// 	}

// 		// 	if (ppColorGradingMode < 3) {
// 		// 		ppColorGradingRedSample = 0;
// 		// 		ppColorGradingGreenSample = 0;
// 		// 		ppColorGradingBlueSample = 0;
// 		// 	} else {
				
// 		// 		cs.basic.contrast = 1f;
// 		// 		cs.basic.saturation = 1f;


// 		// 		if (ppColorGradingMode == 7 || ppColorGradingMode == 8 || ppColorGradingMode == 9) {	//TODO Was gehört alles zu 9??????
// 		// 			float f = 0.5f;

// 		// 			if (ppColorGradingMode == 9) {
// 		// 				cs.channelMixer.red = new Vector3 (f, f, f);
// 		// 				cs.channelMixer.green = new Vector3 (f, f, f);
// 		// 				cs.channelMixer.blue = new Vector3 (f, f, f);
// 		// 			} else {
// 		// 				f = Random.Range (0, 1f);
// 		// 				cs.channelMixer.red = new Vector3 (f, f, f);
// 		// 				f = Random.Range (0, 1f);
// 		// 				cs.channelMixer.green = new Vector3 (f, f, f);
// 		// 				f = Random.Range (0, 1f);
// 		// 				cs.channelMixer.blue = new Vector3 (f, f, f);
// 		// 			}

// 		// 			cs.basic.contrast = Random.Range (0.6f, 2f);
// 		// 		} else {
// 		// 			cs.channelMixer.red = new Vector3 (1f, 0, 0);
// 		// 			cs.channelMixer.green = new Vector3 (0, 1f, 0);
// 		// 			cs.channelMixer.blue = new Vector3 (0, 0, 1f);

// 		// 			if (ppColorGradingMode <= 5 && ppColorGradingMode >= 3 && randomBool (0.5f)) {
// 		// 				int[] i = { (int)Random.Range (0, 15f),  (int)Random.Range (0, 15f),  (int)Random.Range (0, 15f) };
// 		// 				int rf1 = Random.Range (0, i.Length);
// 		// 				int rf2 = Random.Range (0, i.Length);
// 		// 				if (rf1 == rf2)
// 		// 					rf1 = (rf2 + 1) % i.Length;

// 		// 				i [rf1] = i [rf2];
// 		// 			} else {
// 		// 				ppColorGradingRedSample = (int)Random.Range (0, 15f);
// 		// 				ppColorGradingGreenSample = (int)Random.Range (0, 15f);
// 		// 				ppColorGradingBlueSample = (int)Random.Range (0, 15f);
// 		// 			}
// 		// 		}
// 		// 	}

// 		// 	if (ppColorGradingMode == 1 || ppColorGradingMode == 10) {
// 		// 		cs.basic.contrast = 0.5f;
// 		// 	}
// 		// 	if (ppColorGradingMode == 10) {
// 		// 		cs.basic.saturation = 2f;
// 		// 	}
// 		// } else { 
// 		// 	cs = Player.instance.neutralPP.colorGrading.settings;
// 		// }
			
// 		// ppProfile.colorGrading.settings = cs;

// 	}

// 	public static bool randomBool(float f) {
// 		if (f <= 0)
// 			return false;
// 		return (Random.Range (0, 1f) <= f);
// 	}

// 	public void pp(float[] samples) {
// 		//Time
// 		ppTime += Time.deltaTime;
// 		ppTime = ppTime % ppInterval;

// 		// Vignette
// 		// ppProfile.vignette.enabled = ppVignetteEnabled;
// 		// if (ppVignetteStrobo) {
// 		// 	VignetteModel.Settings vs = ppProfile.vignette.settings;
// 		// 	vs.intensity = Mathf.Lerp (0.2f, 0.5f, samples [1]);
// 		// 	ppProfile.vignette.settings = vs;
// 		// }

// 		// Grain
// 		// ppProfile.grain.enabled = ppGrainEnabled;
// 		// GrainModel.Settings gs = ppProfile.grain.settings;
// 		// if (ppGrainStrobo) {
// 		// 	gs.intensity = Mathf.Lerp (0.3f, 1f, samples [0]);
// 		// }
// 		// gs.colored = ppGrainColored;
// 		// gs.size = ppGrainSize;
// 		// ppProfile.grain.settings = gs;



// 		// Color Grading
// 		if (ppColorGradingEnabled) {
// 			// ColorGradingModel.Settings cs = ppProfile.colorGrading.settings;
// 			// cs.tonemapping.neutralBlackIn = 0.02f;
// 			// cs.tonemapping.neutralWhiteIn = 10f;
// 			// cs.tonemapping.neutralWhiteLevel = 5.3f;
// 			// cs.tonemapping.neutralWhiteClip = 10f;

// 			float[] bounds = { 0, 1f };

// 			float f = 0;

// 			/**
// 			 * Color Grading Mode:
// 			 * 0 - Strobo1
// 			 * 1 - Strobo2 (wie 1, anderer Kontrast)
// 			 * 2 - Strobo3, Interpolation von 1 und 2 in Intervallen
// 			 * 3 - Farbspiel1
// 			 * 4 - Farbspiel2 (wie 1, anderer Kontrast)
// 			 * 5 - Farbspiel3, Interpolation von 1 und 2 in Intervallen
// 			 * 6 - schwaches Farbspiel
// 			 * 7 - Tint
// 			 * 8 - Tint High Contrast
// 			 * 9 - SW High Contrast
// 			 * 10 - leichter Farbeffekt
// 			 * */
// 			if(ppColorGradingHardcore) {
// 				bounds [0] = -1f;
// 				bounds [1] = 1f;
// 			} else if(ppColorGradingMode == 6) {
// 				bounds [0] = 1f;
// 				bounds [1] = 2f;
// 			}

// 			// if (ppColorGradingMode == 0 || ppColorGradingMode == 1) {
// 			// 	cs.tonemapping.neutralBlackIn = 0.1f;
// 			// 	cs.tonemapping.neutralWhiteIn = 1f;
// 			// 	cs.tonemapping.neutralWhiteLevel = 0.5f;
// 			// 	cs.tonemapping.neutralWhiteClip = 1f;
// 			// } else if (ppColorGradingMode == 8) {
// 			// 	cs.tonemapping.neutralBlackIn = -0.052f;
// 			// 	cs.tonemapping.neutralWhiteIn = 1f;
// 			// 	cs.tonemapping.neutralWhiteLevel = 10.8f;
// 			// 	cs.tonemapping.neutralWhiteClip = 0.4f;
// 			// } else if (ppColorGradingMode == 5) {
// 			// 	cs.tonemapping.neutralBlackIn = -0.052f;
// 			// }

// 			// if (ppColorGradingMode <= 6) {
// 			// 	f = Mathf.Lerp (bounds [0], bounds [1], samples [ppColorGradingRedSample]);
// 			// 	cs.channelMixer.red = new Vector3 (f, f, f);
// 			// 	f = Mathf.Lerp (bounds [0], bounds [1], samples [ppColorGradingGreenSample]);
// 			// 	cs.channelMixer.green = new Vector3 (f, f, f);
// 			// 	f = Mathf.Lerp (bounds [0], bounds [1], samples [ppColorGradingBlueSample]);
// 			// 	cs.channelMixer.blue = new Vector3 (f, f, f);
// 			// }

// 			// if (ppColorGradingMode == 2 || ppColorGradingMode == 5) {
// 			// 	cs.basic.contrast = 1.5f + 0.5f*Mathf.Sin ((ppTime / ppInterval) * 2 * Mathf.PI);
// 			// }

// 			// ppProfile.colorGrading.settings = cs;
// 		}

// 	}

// 	public bool isActive() {
// 		return active;
// 	}

// 	public void Activate() {
// 		active = true;
// 		videoPlayer [0].targetMaterialRenderer = mainLayer;
// 		videoPlayer [0].Play ();

// 		if (alphaActive) {
// 			videoPlayerAlpha [0].targetMaterialRenderer = alphaLayer;
// 			if(isFading) alphaLayer.material.color = new Color (1f, 1f, 1f, 0.5f);
// 			videoPlayerAlpha [0].Play ();
// 		} else {
// 			alphaLayer.material.color = new Color (1f, 1f, 1f, 0);
// 		}
			
// 		// if (mainShaking) {
// 		// 	if (!mainShake.active)
// 		// 		StartShaking (mainShake);
// 		// } else {
// 		// 	if (mainShake.active)
// 		// 		mainShake.StopShaking ();
// 		// }
// 		// if (alphaShaking) {
// 		// 	if (!alphaShake.active)
// 		// 		StartShaking (alphaShake);
// 		// } else {
// 		// 	if (alphaShake.active)
// 		// 		alphaShake.StopShaking ();
// 		// }

// 	}

// 	public void Deactivate() {
// 		active = false;
// 		videoPlayer [0].Pause ();
// 		videoPlayer [0].targetMaterialRenderer = null;

// 		if (alphaActive) {
// 			videoPlayerAlpha [0].Pause ();
// 			videoPlayerAlpha [0].targetMaterialRenderer = null;
// 		}
// 	}


// 	// void StartShaking(Shake s) {
// 	// 	s.StartShaking (1f, Player.instance.samples, shakeSample);
// 	// }

// 	// // i: 0 --> Screen; 1 --> Layer
// 	// public void ToggleShaking(int i) {
// 	// 	Shake s = mainShake;
// 	// 	if (i != 0) {
// 	// 		s = alphaShake;
// 	// 		alphaShaking = !alphaShaking;
// 	// 	} else {
// 	// 		mainShaking = !mainShaking;
// 	// 	}

// 	// 	if (active) {
// 	// 		if (s.active) {
// 	// 			s.StopShaking ();
// 	// 		} else {
// 	// 			StartShaking (s);
// 	// 		}
// 	// 	}
// 	// }

// 	/**
// 	 * Springt zufällig ein Stück weit von der aktuellen Abspielstelle vor oder zurück.
// 	 * */
// 	public void ToggleReverse(int i) {
// 		if (reverseInProgress)
// 			return;
// 		VideoPlayer[] va = videoPlayer;
// 		bool runningReverse = mainReverse;
// 		if (i != 0) {
// 			va = videoPlayerAlpha;
// 			runningReverse = alphaReverse;
// 			alphaReverse = !alphaReverse;
// 		} else {
// 			mainReverse = !mainReverse;
// 		}
				
// 		VideoPlayer vBefore = va [1];
// 		VideoPlayer vAfter = va [2];
// 		if (runningReverse) {
// 			vBefore = va [2];
// 			vAfter = va [1];
// 		}

// 		if (vBefore != null && vAfter != null) {
// 			float wait = 0.1f;
// 			float t = (float)vAfter.clip.length - (float)vBefore.time;
// 			if (active) {
// 				vAfter.Play ();
// 				StartCoroutine (Reverse (vBefore, vAfter, wait));
// 			}
// 			vAfter.playbackSpeed = vBefore.playbackSpeed;
// 			vAfter.time = t+wait;



// 			va [0] = vAfter;
// 		} else {
// 			if (i != 0) {
// 				alphaReverse = false;
// 			} else {
// 				mainReverse = false;
// 			}
// 		}
// 	}

// 	IEnumerator Reverse(VideoPlayer vBefore, VideoPlayer vAfter, float wait) {
// 		reverseInProgress = true;

// 		yield return new WaitForSeconds (wait);
// 		vBefore.Pause ();
// 		Renderer r = vBefore.targetMaterialRenderer;
// 		vBefore.targetMaterialRenderer = null;
// 		vAfter.targetMaterialRenderer = r;

// 		reverseInProgress = false;
// 	}


// 	/**
// 	 * Springt zufällig ein Stück weit von der aktuellen Abspielstelle vor oder zurück.
// 	 * */
// 	public void Skip(int player, int playerNo, float from, float to)  {
// 		VideoPlayer v = videoPlayer[playerNo];
// 		if (player != 0) {
// 			v = videoPlayerAlpha[playerNo];
// 		}

// 		float f = Random.Range (from, to);
// 		v.time += f;
// 	}

// 	/**
// 	 * player: 0: Main, 1: Alpha
// 	 * playerNo: Nummer des players im Array: 0: Aktiv, 1: vorwärts, 2: rückwärts
// 	 * vc: neuer Clip
// 	 * 
// 	 * Springt mit Player zu zufälliger Abspielzeit.
// 	 * */
// 	public void JumpToRandomTime(int player, int playerNo) {
// 		VideoPlayer v = videoPlayer[playerNo];
// 		if (player != 0) {
// 			v = videoPlayerAlpha[playerNo];
// 		}

// 		JumpToRandomTime (v);
// 	}

// 	/**
// 	 * player: 0: Main, 1: Alpha
// 	 * playerNo: Nummer des players im Array: 0: Aktiv, 1: vorwärts, 2: rückwärts
// 	 * vc: neuer Clip
// 	 * 
// 	 * Setzt v.time an zufällige Stelle unter Auslassung des letzten Bereichs im Clip (Länge in videoEndBuffer festgelegt)
// 	 * */
// 	void JumpToRandomTime(VideoPlayer v) {
// 		float f = Random.Range (0, (float)v.clip.length-videoEndBuffer);
// 		v.time = f;
// 	}

// 	public void TogglePlaybackSpeed(int player, int playerNo) {
// 		VideoPlayer v = videoPlayer[playerNo];
// 		if (player != 0) {
// 			v = videoPlayerAlpha[playerNo];
// 		}

// 		if (v.playbackSpeed > 1f) {
// 			v.playbackSpeed = 1f;
// 		} else {
// 			v.playbackSpeed = 9f;
// 		}
// 	}

// 	public void SetPlaybackSpeedRandom(int player, int playerNo) {
// 		SetPlaybackSpeed(player, playerNo, Random.Range(randomPlaybackSpeedBounds[0], randomPlaybackSpeedBounds[1]));
// 	}

// 	public void SetPlaybackSpeed(int player, int playerNo, float f) {
// 		VideoPlayer v = videoPlayer[playerNo];
// 		if (player != 0) {
// 			v = videoPlayerAlpha[playerNo];
// 		}

// 		v.playbackSpeed = f;
// 	}
		
// 	public void ToggleAlpha() {
// 		if (!alphaActive) {
// 			videoPlayerAlpha [0].clip = RandomClip (null);
// 			if (active) {
// 				videoPlayerAlpha [0].Play ();
// 				alphaLayer.material.color = new Color (1f, 1f, 1f, 0.5f);
// 				videoPlayerAlpha [0].targetMaterialRenderer = alphaLayer;
// 			}
// 			SetPlaybackSpeedRandom (1, 0);
// 			JumpToRandomTime (videoPlayerAlpha [0]);
// 		} else {
// 			if (active) {
// 				videoPlayerAlpha [0].Pause ();
// 				alphaLayer.material.color = new Color (1f, 1f, 1f, 0f);
// 				videoPlayerAlpha [0].targetMaterialRenderer = null;
// 			}
// 		}

// 		alphaActive = !alphaActive;
// 	}

// 	public void ToggleAlphaVisible() {
// 		if (active) {
// 			if (alphaLayer.material.color.a > 0) {
// 				alphaLayer.material.color = new Color (1f, 1f, 1f, 0f);
// 			} else {
// 				alphaLayer.material.color = new Color (1f, 1f, 1f, 0.5f);
// 			}
// 		}
// 	}

// 	/**
// 	 * Ändern zufällig den Main-Clip. Der des Alpha-Players kann nur durch ToggleAlpha neu geśetzt werden.
// 	 * */
// 	public void ChangeClipRandom() {
// 		VideoClip vc = RandomClip(videoPlayer[1].clip);	// neuen Clip holen unter Ausschluss des aktuell verwendeten
// 		VideoClip vr = null;
// 		Player.instance.clipReverseDict.TryGetValue (vc, out vr);

// 		// Wechsel ausführen
// 		ChangeClip(0, 1, vc);
// 		ChangeClip(0, 2, vr);
// 	}

// 	/**
// 	 * player: 0: Main, 1: Alpha
// 	 * playerNo: Nummer des players im Array: 0: Aktiv, 1: vorwärts, 2: rückwärts
// 	 * vc: neuer Clip
// 	 * 
// 	 * Ändert den Clip eines beliebigen Videoplayers.
// 	 * */
// 	void ChangeClip(int player, int playerNo, VideoClip vc) {
// 		VideoPlayer v = videoPlayer[playerNo];
// 		if (player != 0) {	// Ist es Alpha?
// 			v = videoPlayerAlpha[playerNo];
// 		}

// 		v.time = 0;
// 		v.clip = vc;
// 		if (active && (
// 		        (player == 0 && playerNo == 2 && mainReverse)
// 		        || (player == 0 && playerNo == 1 && !mainReverse)
// 		        || (player == 1 && playerNo == 2 && alphaReverse)
// 		        || (player == 1 && playerNo == 1 && !alphaReverse)
// 		    )) {
// 			v.Play ();
// 		} else if (playerNo == 2) {
// 			StartCoroutine (InitNewClip (v));
// 		}
// 		JumpToRandomTime (v);
// 	}

// 	/**
// 	 * TODO: hilft das wirklich? anscheinend nicht
// 	 * Wenn ein Clip nicht angespielt wird, macht er Probleme beim ersten Umschalten druch ToogleReverse.
// 	 * Der Aufruf dieser Routine für seinen Player verhindert den Fehler.
// 	 * */
// 	IEnumerator InitNewClip(VideoPlayer v) {
// 		yield return new WaitForSeconds (0.1f);
// 		v.Play ();
// 		yield return new WaitForSeconds (0.1f);
// 		v.Pause ();
// 		yield return null;
// 	}

// 	VideoPlayer CreateVideoPlayer(VideoClip avoid, VideoClip c) {
// 		VideoPlayer v = (VideoPlayer) gameObject.AddComponent<VideoPlayer> ();

// 		InitVideoPlayer (v);

// 		if (c != null) {
// 			v.clip = c;
// 		} else {
// 			v.clip = RandomClip (avoid);
// 		}

// 		return v;
// 	}

// 	/**
// 	 * Gibt einen zufälligen, vorwärtlaufenden Clip aus dem clips Array von Player zurück. Der Clip avoid wird dabei (wenn != null) nicht in die Zufallsauswahl eingeschlossen.
// 	 * */
// 	VideoClip RandomClip(VideoClip avoid) {
// 		int i = Random.Range(0, Player.instance.clips.Length);
// 		if (avoid != null && avoid.Equals(Player.instance.clips[i])) {
// 			i = (i+1)%Player.instance.clips.Length;
// 		}
// 		return Player.instance.clips[i];
// 	}

// 	/**
// 	 * 
// 	 * */
// 	void InitVideoPlayer(VideoPlayer v) {
// 		v.playOnAwake = false;
// 		v.isLooping = true;
// 		v.audioOutputMode = VideoAudioOutputMode.None;
// 		v.renderMode = VideoRenderMode.MaterialOverride;
// 	}
// }
