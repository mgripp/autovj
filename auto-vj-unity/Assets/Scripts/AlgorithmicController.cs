﻿using System.Collections;
using UnityEngine;

public class AlgorithmicController : MonoBehaviour
{
    Coroutine clipChanger;

    //IEnumerator Start()
    //{
    //    yield return new WaitForSeconds(.1f);

    //    if (Settings.Instance.changeClipsInBackground)
    //        clipChanger = StartCoroutine(ClipChanger());

    //    int nonSwitchActions = 0;
    //    int action = -1;

    //    int[] actions = new int[3];
    //    actions[0] = PlayerManager.ToggleReverseAction;
    //    actions[1] = PlayerManager.TogglePlaybackSpeedAction;
    //    actions[2] = PlayerManager.SkipAction;

    //    while(false)
    //    {
    //        int beatCount = AudioAnalyzer.Instance.FetchBeatCount();

    //        if (AudioAnalyzer.Instance.volumeAmplitudeAverage > 2.5f || Random.Range(0, 1f) > .8f)
    //        {
    //            //if(beatCount == 0 || beatCount == 4)
    //            if (AudioAnalyzer.Instance.hits > AudioAnalyzer.Instance.hitsBeat)
    //            {
    //                if (nonSwitchActions == 0 && Random.Range(0, 1f) > .05f)
    //                {
    //                    action = -1;
    //                    PlayerManager.Instance.Action(index: PlayerManager.SwitchToNextPlayerAction);
    //                }
    //                else
    //                {
    //                    nonSwitchActions++;

    //                    if (action < 0)
    //                        action = Random.Range(0, actions.Length - 1);

    //                    float[] args = { 2f, -4f, 5f };
    //                    PlayerManager.Instance.Action(index: actions[action], args: args);
    //                    //LogoAnimator.Instance.Action();

    //                    if (nonSwitchActions > 32)
    //                        nonSwitchActions = 0;
    //                }

    //                yield return new WaitForSeconds(.1f);
    //            }
    //        }
    //        else
    //        {
    //            PlayerManager.Instance.SetPlaybackSpeed(1f);
    //            PlayerManager.Instance.StopShaking();
    //        }

    //        yield return new WaitForEndOfFrame();
    //    }
    //}

    IEnumerator ClipChanger()
    {
        yield return new WaitForSeconds(Settings.Instance.changeClipsInBackgroundInterval);

        while(true)
        {
            if(!Settings.Instance.changeClipsInBackground)
            {
                yield return new WaitForSeconds(10);
            }
            else
            {
                PlayerManager.Instance.ChangeClipInBackground();
                yield return new WaitForSeconds(Settings.Instance.changeClipsInBackgroundInterval);
            }
        }
    }
}
