﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioAnalyzer : MonoBehaviour
{
    public static AudioAnalyzer Instance;

    [SerializeField] AudioDisplay audioDisplay;

    [SerializeField] int frequencySamplesCountBase2 = 8;
    [SerializeField] Audioinput audioInput;
    [SerializeField] enum Audioinput { Mikrofon, Stereomix };
    [SerializeField] float[] bpmBounds = { 50f, 200f };
    [SerializeField] AudioSource audiosource;
    [SerializeField] UDPReceive udpMidiReceive;
    [SerializeField] UDPSend udpMidiSend;
    [SerializeField] int actionBeatsStart = 12;
    [SerializeField] public int hitsBeat = 30;
    [SerializeField] float introDuration = 60f;     // Zeit in s, die das Intro der abgespielten Stücke etwa dauert
    [SerializeField] int bpmGuessMaxSamples = 200;      // maximale Anzahl an Beats, die benötigt werden, bis BPM sicher ist
    [SerializeField] int bpmAdaptStep = 2;				// Schritte, in denen pro Korrektur hitsBeat manipuliert wird

    [HideInInspector] public bool actionAmpReached = false;
    [HideInInspector] public float averageBeatTime = 0;	// Durchschnittlicher Abstand zwischen den Beats
    [HideInInspector] public float bpm = 0;
    [HideInInspector] public int hits = 0;
    [HideInInspector] public Indicator volumeIndicator;
    [HideInInspector] public Indicator volumeIndicatorSoft;
    [HideInInspector] public List<Indicator> frequencyIndicators = new List<Indicator>();
    [HideInInspector] public int FrequencySamplesCount => frequencyIndicators.Count;

    float[] samples;
    int beatCount = -1;
    float beatTime = 0;
    List<float> averageBeatTimeSamples;
    int beatTimeSamples = 0;
    float beatTimeSum = 0;
    float time = 0;
    float intensityTime = 0; // Zeitzähler, der benutzt wird, um intensityLevel mit Delay zu verändern
    bool introRunning = true; // Musikstück noch im Intro?
    int intensityLevel = 2;	// Intesität in Stufen ausgedrückt: 0 --> lahm, nix dolles passiert (Zwischenpassage vor Drop, Intro, ...) 1 --> Refrain etc. 2 --> richtig hart am abgehen
    float beatTapTime = 0;
    int beatTaps = 0;
    float beatTapSum = 0;
    bool beatTapped = false;
    bool beatOnCooldown = false;

    public float Volume { get; private set; } = 0;

    void Start()
    {
        averageBeatTimeSamples = new List<float>();

        int samplesCount = GetSamplesNumber();
        for (int i = 0; i < samplesCount; i++)
            frequencyIndicators.Add(new Indicator(scale: Mathf.Lerp(.4f, 10f, (float)i / (float)samplesCount)));

        audioDisplay.Inititalize(FrequencySamplesCount);

        volumeIndicator = new Indicator(scale: 5f / (float)samplesCount);
        volumeIndicatorSoft = new Indicator(scale: 5f / (float)samplesCount, averageRangeScale: 10f);

        print("Audio Devices: *************************");
        foreach (string device in Microphone.devices)
            print(device);
        print("               *************************");

        SetInput(audioInput.ToString());

        //panel.SetBPMText(bpm);

        Instance = Instance ?? this;
    }

    void Update()
    {
        beatTime += Time.deltaTime;
        time += Time.deltaTime;
        intensityTime += Time.deltaTime;
        beatTapTime += Time.deltaTime;

        AnalyzeFrequencySpectrum();
        bool beat = GetBeat();
        if (beat) Beat();
    }

    void Beat()
    {
        if (beatOnCooldown)
            return;
        AudioDisplay.Instance.DisplayBeat();
        DecisionMaker.Instance.Beat();
        StartCoroutine(BeatCooldown());
    }

    public float GetSample(float position) => samples[(int)(position * samples.Length)];

    IEnumerator BeatCooldown()
    {
        beatOnCooldown = true;
        yield return new WaitForSecondsRealtime(Settings.Instance.beatCooldown);
        beatOnCooldown = false;
    }

    bool GetBeat() => GetHitNumber() > Settings.Instance.BeatHits * FrequencySamplesCount;

    int GetHitNumber()
    {
        int hitCount = 0;
        foreach (Indicator indicator in frequencyIndicators)
            if (indicator.Hit)
                hitCount++;
        return hitCount;
    }

    public int GetSamplesNumber()
    {
        return (int)Mathf.Pow(2, frequencySamplesCountBase2);
    }

    void AnalyzeFrequencySpectrum()
    {
        samples = new float[FrequencySamplesCount];
        audiosource.GetSpectrumData(samples, 0, FFTWindow.BlackmanHarris);
        Volume = 0;
        for (int i = 0; i < samples.Length; i++)
        {
            samples[i] *= Settings.Instance.volume * Settings.Instance.volumeCurve.Evaluate((float)i / (float)samples.Length);
            Volume += samples[i];
            Indicator currentIndicator = frequencyIndicators[i];
            currentIndicator.Value = samples[i];
            audioDisplay.DisplayFequencySpectrum(currentIndicator, i);
            samples[i] /= Settings.Instance.sampleMaxValue;
        }
        volumeIndicator.Value = Volume;
        volumeIndicatorSoft.Value = Volume;
        audioDisplay.DisplayVolume(volumeIndicator, volumeIndicatorSoft);
    }

    void CheckBeatMidi()
    {
        bool fullBeat = false;

        byte[] packet = udpMidiReceive.GetLatestUDPPacket();
        if (packet != null && packet.Length > 0)
        {
            // string s = "";
            // foreach (byte b in packet)
            // {
            //     s += b.ToString();
            // }
            // print(s);
            // beat = message;
            beatCount = packet[0];
            // print(beat_count);
            if (beatCount == 0)
            {
                fullBeat = true;
            }
        }

        if (fullBeat)
        {
            if (beatTimeSamples < bpmGuessMaxSamples)
            {
                beatTimeSum += beatTime;
                beatTimeSamples++;

                float avNew = beatTimeSum / beatTimeSamples;
                averageBeatTimeSamples.Add(avNew);
                averageBeatTimeSamples.Sort();
                if (averageBeatTimeSamples.Count > 1)
                    averageBeatTime = averageBeatTimeSamples[(averageBeatTimeSamples.Count / 2)];
                if (averageBeatTime > 0)
                {
                    bpm = 60f / averageBeatTime;
                    //panel.SetBPMText(bpm);
                }

                beatTime = 0;

                // Tap auswerten, sofern vorhanden
                if (beatTapped)
                {
                    beatTapped = false;
                    beatTapSum += beatTapTime;
                }
            }
        }
    }

    /**
    * Returns current value of beat and resets it to false. This way beat stays true after a beat happened until something fetches it.
    * This should happen frequently before the next beat happens, preferably in a Monobehaviour's Update function.
    **/
    public int FetchBeatCount() {
        int _beatCount = beatCount;
        beatCount = -1;
        return _beatCount;
    }

    void SetInput(string audioInput)
    {
        // TODO more efficient
        // TODO adapt amp
        float startTime = Time.time;
        string audioInputName = null;
        foreach (string device in Microphone.devices)
        {
            if (device.Contains(audioInput))
            {
                audioInputName = device;
                break;
            }
        }
        if (audioInputName == null)
        {
            Debug.LogWarningFormat("Selected audio input could not be found: {0}", audioInput);
            audioInputName = Audioinput.Mikrofon.ToString();
        }
        audiosource.clip = Microphone.Start(audioInputName, true, 10, 44100);
        audiosource.loop = true;
        float loops = 0;
        while(Microphone.GetPosition(null) <= 0)
        {
            if (loops > 40000)
            {
                Debug.LogWarningFormat("Tried to get microphone positon for {0} loops without success", loops);
                break;
            }
            loops++;
        }
        float latency = Time.time - startTime;
        audiosource.time = latency;
        audiosource.Play();
        Debug.LogFormat("Set audio input to '{0}' with latency of {1}s", audioInputName, latency);
    }

    public void SetInput(bool stereoMix)
    {
        if (stereoMix)
            SetInput("Stereomix");
        else
            SetInput("Mikrofon");
    }
}
