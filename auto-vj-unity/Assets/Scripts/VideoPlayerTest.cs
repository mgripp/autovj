﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;

public class VideoPlayerTest : MonoBehaviour
{
    public string url;
    public GameObject camera;

    // Start is called before the first frame update
    void Start()
    {
        VideoPlayer videoPlayer = camera.AddComponent<VideoPlayer>();
        videoPlayer.url = url;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
