﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;
using UnityEngine.Rendering.PostProcessing;

public class VideoPlayerWrapper : MonoBehaviour
{
    static float videoEndBuffer = 10; // Zeit in s, die am Ende eines Clips ausgelassen werden, wenn zufällig in ihm eine Zeit angespielt wird

    [SerializeField] VideoPlayer player;
    [SerializeField] Camera camera;
    [SerializeField] public PostProcessAnimatorStack postProcessAnimators;

    public bool HasReverse => videoClip.HasReverse;

    VideoPlayer playerReverse;
    VideoClipWrapper videoClip;
    VideoPlayer activeVideoPlayer;
    VideoLayer layer;

    bool active = false;
    bool shaking = false;
    public bool mainReverse = false;
    bool alphaReverse = false;
    public bool alphaActive = false;
    bool isFading = false;
    bool runningReverse = false;
    bool toggleReverseInProgress = false;

    private void Awake()
    {
        player.targetCamera = camera;

        playerReverse = gameObject.AddComponent<VideoPlayer>();
        playerReverse.targetCamera = camera;
        playerReverse.isLooping = player.isLooping;
        playerReverse.playOnAwake = player.playOnAwake;
        playerReverse.skipOnDrop = player.skipOnDrop;
        playerReverse.renderMode = player.renderMode;
        playerReverse.audioOutputMode = player.audioOutputMode;

        activeVideoPlayer = player;
    }

    public static bool randomBool(float f)
    {
        if (f <= 0)
            return false;
        return (Random.Range(0, 1f) <= f);
    }

    public void Activate()
    {
        Play();
        camera.enabled = true;
        activeVideoPlayer.targetCamera = camera;
        postProcessAnimators.enabled = true;
    }

    public void Deactivate()
    {
        camera.enabled = false;
        activeVideoPlayer.targetCamera = null;
        postProcessAnimators.enabled = false;
        Pause();
    }

    public void Play() => activeVideoPlayer.Play();
    public void Pause() => activeVideoPlayer.Pause();

    // TODO: Make Change clip that actually changes clip, this here is more of an init
    public void ChangeClip(VideoClipWrapper clipWrapper)
    {
        videoClip = clipWrapper;
        activeVideoPlayer = player;
        SetPlayerURL(player, videoClip.url);
        print(videoClip.url);
        print(HasReverse);
        if(HasReverse)
            SetPlayerURL(playerReverse, videoClip.urlReverse);
        runningReverse = false;
        StopShaking();
        player.Prepare();
        playerReverse.Prepare();
    }

    void SetPlayerURL(VideoPlayer player, string url)
    {
        try
        {
            player.url = url;
        }
        catch
        {
            Debug.LogError($"Could not load video file: '{url}'");
        }
    }

    // Slow! Use only on init.
    float JumpToRandomTime(VideoPlayer player)
    {
        float f = Random.Range(0, (float)player.length - videoEndBuffer);
        player.time = f;
        Debug.Log($"Jumped To Time: {player.time}. Player length: {player.length}, target: {f}");
        return f;
    }

    // Slow! Use only on init.
    public float JumpToRandomTime() => JumpToRandomTime(activeVideoPlayer);

    public float Skip(float minTime, float maxTime)
    {
        float skipTime = Random.Range(minTime, maxTime);
        activeVideoPlayer.time += skipTime;
        return skipTime;
    }

    public bool ToggleReverse()
    {
        if (!HasReverse || toggleReverseInProgress)
            return runningReverse;

        StartCoroutine(ToggleReverseRoutine());

        return runningReverse;
    }

    IEnumerator ToggleReverseRoutine()
    {
        VideoPlayer newPlayer;
        if (runningReverse)
            newPlayer = player;
        else
            newPlayer = playerReverse;

        float time = (float)newPlayer.length - (float)activeVideoPlayer.time;
        newPlayer.time = time;

        Deactivate();
        activeVideoPlayer = newPlayer;
        activeVideoPlayer.Play();

        yield return new WaitForSeconds(.07f);

        Activate();
        runningReverse = !runningReverse;

        yield return null;
    }

    public void SetPlaybackSpeed(float speed)
    {
        speed = Mathf.Clamp(speed, 1f, 10f);
        player.playbackSpeed = speed;
        playerReverse.playbackSpeed = speed;
    }

    public float SetPlaybackSpeedRandom()
    {
        float speed = Random.Range(1f, 10f);
        SetPlaybackSpeed(speed);
        return speed;
    }

    public void TogglePlaybackSpeed()
    {
        if (player.playbackSpeed > 1f)
        {
            SetPlaybackSpeed(1f);
        }
        else
        {
            SetPlaybackSpeed(9f);
        }
    }

    // Make this wrapper's layer shake in case it is in shaking mode, otherwise stop (might still be in shaking mode from previous wrapper)
    void UpdateShaking()
    {
        if (active)
        {
            if (shaking)
            {
                if (AudioAnalyzer.Instance == null)
                    return;
                //PlayerManager.Instance.mainLayer.StartShaking(1f, AudioAnalyzer.Instance.samples, 1);
            } else
                PlayerManager.Instance.mainLayer.StopShaking();
        }
    }

    public void StartShaking()
    {
        shaking = true;
        UpdateShaking();
    }

    public void StopShaking()
    {
        shaking = false;
        UpdateShaking();
    }

    public bool ToggleShaking()
    {
        shaking = Settings.Instance.shakeEnabled && !shaking;
        UpdateShaking();
        return shaking;
    }

    public void Flash() => postProcessAnimators.Flash();

    public void Randomize()
    {
        // randomPP ();
        // if (randomBool(0.2f)) ToggleAlpha ();
        //shaking = Settings.Instance.shakeEnabled && randomBool(0.6f);
        //UpdateShaking();
        JumpToRandomTime();
        postProcessAnimators.ClearAnimators();
        postProcessAnimators.AddRandomAnimator();
    }

    public void RandomizeDelayed() => StartCoroutine(RandomizeRoutine());

    IEnumerator RandomizeRoutine()
    {
        while (!activeVideoPlayer.isPrepared || !activeVideoPlayer.isPlaying)
            yield return new WaitForEndOfFrame();
        Randomize();
    }

    public void randomPP()
    {
        // ppVignetteEnabled = randomBool (0.33f);
        // ppVignetteStrobo = randomBool (0.75f);

        // ppGrainEnabled = randomBool (0.5f);
        // ppGrainSize = Random.Range (0.3f, 3f);
        // ppGrainColored = randomBool (0.5f);
        // ppGrainStrobo = randomBool (0.33f);

        // ppColorGradingEnabled = randomBool(0.4f);
        // //ppColorGradingEnabled = true;
        // //ppColorGradingMode = (int)((ppColorGradingMode + 1) % 11);
        // ppColorGradingMode = (int) Random.Range (0, 11f);
        // if(ppColorGradingMode < 4 && randomBool(0.75f)) ppColorGradingMode = (int) Random.Range (4, 11f);	// Die ersten 4 Modi sind sehr ähnlich, so wird ein zu häufiges vorkommen verhindert
        // ppColorGradingHardcore = false;//randomBool(0.1f);

        //print ("Mode: " + ppColorGradingMode);

        // if (ppColorGradingEnabled) {
        // 	cs.basic.postExposure = 5f;
        // 	if (ppColorGradingMode == 2 || ppColorGradingMode == 3) { 
        // 		cs.basic.postExposure = 6f;
        // 	} else if (ppColorGradingMode == 4) {
        // 		cs.basic.postExposure = 8f;
        // 	} else if (ppColorGradingMode == 8) {
        // 		cs.basic.postExposure = 2.5f;
        // 	}

        // 	if (ppColorGradingMode < 3) {
        // 		ppColorGradingRedSample = 0;
        // 		ppColorGradingGreenSample = 0;
        // 		ppColorGradingBlueSample = 0;
        // 	} else {

        // 		cs.basic.contrast = 1f;
        // 		cs.basic.saturation = 1f;


        // 		if (ppColorGradingMode == 7 || ppColorGradingMode == 8 || ppColorGradingMode == 9) {	//TODO Was gehört alles zu 9??????
        // 			float f = 0.5f;

        // 			if (ppColorGradingMode == 9) {
        // 				cs.channelMixer.red = new Vector3 (f, f, f);
        // 				cs.channelMixer.green = new Vector3 (f, f, f);
        // 				cs.channelMixer.blue = new Vector3 (f, f, f);
        // 			} else {
        // 				f = Random.Range (0, 1f);
        // 				cs.channelMixer.red = new Vector3 (f, f, f);
        // 				f = Random.Range (0, 1f);
        // 				cs.channelMixer.green = new Vector3 (f, f, f);
        // 				f = Random.Range (0, 1f);
        // 				cs.channelMixer.blue = new Vector3 (f, f, f);
        // 			}

        // 			cs.basic.contrast = Random.Range (0.6f, 2f);
        // 		} else {
        // 			cs.channelMixer.red = new Vector3 (1f, 0, 0);
        // 			cs.channelMixer.green = new Vector3 (0, 1f, 0);
        // 			cs.channelMixer.blue = new Vector3 (0, 0, 1f);

        // 			if (ppColorGradingMode <= 5 && ppColorGradingMode >= 3 && randomBool (0.5f)) {
        // 				int[] i = { (int)Random.Range (0, 15f),  (int)Random.Range (0, 15f),  (int)Random.Range (0, 15f) };
        // 				int rf1 = Random.Range (0, i.Length);
        // 				int rf2 = Random.Range (0, i.Length);
        // 				if (rf1 == rf2)
        // 					rf1 = (rf2 + 1) % i.Length;

        // 				i [rf1] = i [rf2];
        // 			} else {
        // 				ppColorGradingRedSample = (int)Random.Range (0, 15f);
        // 				ppColorGradingGreenSample = (int)Random.Range (0, 15f);
        // 				ppColorGradingBlueSample = (int)Random.Range (0, 15f);
        // 			}
        // 		}
        // 	}

        // 	if (ppColorGradingMode == 1 || ppColorGradingMode == 10) {
        // 		cs.basic.contrast = 0.5f;
        // 	}
        // 	if (ppColorGradingMode == 10) {
        // 		cs.basic.saturation = 2f;
        // 	}
        // } else { 
        // 	cs = Player.Instance.neutralPP.colorGrading.settings;
        // }

        // ppProfile.colorGrading.settings = cs;

    }

    public override string ToString() => videoClip.ToString();
}
