﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DecisionMaker : MonoBehaviour
{
    public static DecisionMaker Instance { get; private set; }
    private void Awake() => Instance = Instance ?? this;

    public bool hitOperatorOr = true;

    Indicator[] hitMetrics;
    List<Indicator> hitMetricsUsed;

    Sequencer sequencer;

    bool beatOnCooldown = false;
    bool trackRunningBefore = false;

    void Start()
    {
        hitMetrics = new Indicator[]
        {
            Backend.Instance.Volume,
            Backend.Instance.VolumeSoft,
            Backend.Instance.NoveltyEnergy,
            Backend.Instance.NoveltySpectral,
            Backend.Instance.ZeroCrossingRate,
            Backend.Instance.KickDrum,
            Backend.Instance.HiHat,
            Backend.Instance.SnareDrum,
        };
        hitMetricsUsed = new List<Indicator>
        {
            Backend.Instance.SnareDrum,
        };

        sequencer = Sequencer.GetRandomSequencer();
        StartCoroutine(ClipChangeRoutine());
    }

    void Update()
    {
        bool trackRunning = TrackRunning();

        if (trackRunning)
            if (BeatDetected())
                Beat();
        
        CheckIfTrackIsRunningChanged(trackRunning);
        trackRunningBefore = trackRunning;
    }

    void CheckIfTrackIsRunningChanged(bool trackRunning)
    {
        if (!trackRunning && trackRunningBefore)
            VideoPlayerManager.Instance.CalmAllPlayers();
    }

    bool BeatDetected()
    {
        bool result = !hitOperatorOr;
        foreach (Indicator i in hitMetricsUsed)
        {
            if (i == null)
                continue;
            if (hitOperatorOr)
                result = result || i.Hit;
            else
                result = result && i.Hit;
            if (result)
                break;
        }
        return result;
    }

    public bool TrackRunning() => Backend.Instance.VolumeSoft.Value > Settings.Instance.minVolume;

    public void Beat()
    {
        if (beatOnCooldown)
            return;
        //if (AudioAnalyzer.Instance.Volume > Settings.Instance.minVolume)
        //    VideoPlayerManager.Instance.SwitchToNextMainPlayer();
        AudioDisplay.Instance.DisplayBeat();
        //VideoPlayerManager.Instance.SwitchToNextMainPlayer();

        if (sequencer.CallNextAction())
            sequencer = Sequencer.GetRandomSequencer();

        //Actions.CallRandomAction();
        //VideoPlayerManager.Instance.Flash();


        StartCoroutine(BeatCooldown());
    }

    public void AddRemoveRandomHitIndicator()
    {
        hitOperatorOr = Random.value > .5f;
        bool remove = Random.value > .5f;
        if (remove)
        {
            if (hitMetricsUsed.Count < 2)
                return;
            Indicator i = hitMetricsUsed[Random.Range(0, hitMetricsUsed.Count)];
            RemoveHitIndicator(i);
        }
        else
        {
            Indicator i = hitMetrics[Random.Range(0, hitMetrics.Length)];
            AddHitIndicator(i);
        }
        Panel.Instance.UpdateHitOperator();
    }

    public void AddHitIndicator(Indicator indicator)
    {
        if (!hitMetricsUsed.Contains(indicator))
            hitMetricsUsed.Add(indicator);
        HitToggle.UpdateInstances();
    }

    public void RemoveHitIndicator(Indicator indicator)
    {
        if (hitMetricsUsed.Contains(indicator))
            hitMetricsUsed.Remove(indicator);
        HitToggle.UpdateInstances();
    }

    public bool UsesHitIndicator(Indicator indicator) => hitMetricsUsed.Contains(indicator);

    IEnumerator BeatCooldown()
    {
        beatOnCooldown = true;
        yield return new WaitForSecondsRealtime(Settings.Instance.beatCooldown);
        beatOnCooldown = false;
    }

    IEnumerator ClipChangeRoutine()
    {
        while (true)
        {
            yield return new WaitForSeconds(Settings.Instance.clipChangeInterval);
            VideoPlayerManager.Instance.ChangeClipInBackground();
        }
    }
}
