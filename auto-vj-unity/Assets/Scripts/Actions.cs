﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Actions : MonoBehaviour
{
    public static Actions Instance;

    public delegate void Action();

    static Dictionary<string, Action> actionsDict;
    static Action[] actions;
    static Action[] actionsRandom;
    static public Action SwitchToNextMainPlayer;
    static public Action SwitchMainPlayerRandom;
    static public Action ToggleReverse;
    static public Action TogglePlaybackSpeed;
    static public Action ToggleShaking;
    static public Action Flash;

    private void Awake() => Instance = this ?? Instance;

    private void Start()
    {
        SwitchToNextMainPlayer = VideoPlayerManager.Instance.SwitchToNextMainPlayer;
        SwitchMainPlayerRandom = VideoPlayerManager.Instance.SwitchMainPlayerRandom;
        ToggleReverse = VideoPlayerManager.Instance.ToggleReverse;
        TogglePlaybackSpeed = VideoPlayerManager.Instance.TogglePlaybackSpeed;
        ToggleShaking = VideoPlayerManager.Instance.ToggleShaking;
        Flash = VideoPlayerManager.Instance.Flash;

        actionsDict  = new Dictionary<string, Action>
        {
            { "SwitchToNextPlayer", new Action (VideoPlayerManager.Instance.SwitchToNextMainPlayer) },
            { "SwitchMainPlayerRandom", new Action (VideoPlayerManager.Instance.SwitchMainPlayerRandom) },
            { "ToggleReverse", new Action (VideoPlayerManager.Instance.ToggleReverse) },
            { "TogglePlaybackSpeed", new Action (VideoPlayerManager.Instance.TogglePlaybackSpeed) },
            { "ToggleShaking", new Action (VideoPlayerManager.Instance.ToggleShaking) },
            { "Pass", new Action (PassAction) },
        };

        actions = new Action[]
        {
            new Action (VideoPlayerManager.Instance.SwitchToNextMainPlayer),
            new Action (VideoPlayerManager.Instance.SwitchMainPlayerRandom),
            new Action (VideoPlayerManager.Instance.ToggleReverse),
            new Action (VideoPlayerManager.Instance.TogglePlaybackSpeed),
            new Action (VideoPlayerManager.Instance.ToggleShaking),
            new Action (PassAction),
        };

        actionsRandom = new Action[]
        {
            new Action (VideoPlayerManager.Instance.SwitchToNextMainPlayer),
            new Action (VideoPlayerManager.Instance.ToggleReverse),
            new Action (VideoPlayerManager.Instance.TogglePlaybackSpeed),
            new Action (PassAction),
        };
    }

    void PassAction() { }

    public static void CallAction(int index) => actions[index]();
    public static void CallAction(string name) => actionsDict[name]();
    public static void CallRandomAction() => actionsRandom[(int)Random.Range(0, actionsRandom.Length)]();
}
