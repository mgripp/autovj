﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Indicator
{
    public bool Hit { get; private set; } = false;
    Queue<float> lastSamples = new Queue<float>();

    float value = 0;
    float bouncerSpeed = .1f;
    float scale = 1f;
    float averageRangeScale = 1f;
    float hitTimer = 0;
    float lastRefreshedTime = 0;

    public Indicator(float bouncerSpeed = .1f, float scale = 1f, float averageRangeScale = 1f)
    {
        Initialize(bouncerSpeed: bouncerSpeed, scale: scale, averageRangeScale: averageRangeScale);
    }

    public float BouncerValue { get; private set; } = 0;
    public float Value
    {
        set => Refresh(value);
        get => value;
    }

    public void Initialize(float bouncerSpeed = .1f, float scale = 1f, float averageRangeScale = 1f)
    {
        this.scale = scale;
        this.bouncerSpeed = bouncerSpeed;
        this.averageRangeScale = averageRangeScale;
        lastSamples = new Queue<float>();
    }

    void Refresh(float newValue)
    {
        float now = Time.time;
        float deltaTime = now - lastRefreshedTime;
        lastRefreshedTime = now;

        hitTimer -= deltaTime;
        Hit = false;

        BouncerValue -= deltaTime * this.bouncerSpeed;

        if (lastSamples.Count >= Settings.Instance.AverageRange * averageRangeScale)
            lastSamples.Dequeue();
        lastSamples.Enqueue(newValue);

        float sum = 0;
        foreach (float sample in lastSamples)
            sum += sample;

        value = scale * sum / lastSamples.Count;
        if (BouncerValue < value)
        {
            BouncerValue = value;
            if (hitTimer <= 0 && value > Settings.Instance.indicatorHitMinValue)
                Hit = true;
            hitTimer = Settings.Instance.indicatorHitCooldown;
        }
    }
}
