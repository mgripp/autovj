﻿using System.Collections;
using System.Collections.Generic;
using System;
using System.Diagnostics;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Text;
using UnityEngine;

public class Backend : MonoBehaviour
{
    static public Backend Instance;

    // UDP
    // source: https://stackoverflow.com/questions/37131742/how-to-use-udp-with-unity-methods
    [SerializeField] bool udp;
    static UdpClient udpClient;
    Thread udpThread;
    static readonly object udpLockObject = new object();
    string udpMessage = "";
    bool udpPrecessData = false;

    // CMD
    CommandPrompt commandPrompt = new CommandPrompt();
    Process process;
    string pythonScriptPath = "/../../python/audio_analyze_realtime.py";
    //string pythonScriptPath = "/../../python/test.py";
    string virtualEnvironmentPath = "/../../python/venv/";

    // Indicators
    public Indicator Volume { get; private set; } = new Indicator(bouncerSpeed: .01f, scale: .003f);
    public Indicator VolumeSoft { get; private set; } = new Indicator(bouncerSpeed: .001f, averageRangeScale: 100);
    public Indicator SnareDrum { get; private set; } = new Indicator(bouncerSpeed: .8f);
    public Indicator HiHat { get; private set; } = new Indicator(bouncerSpeed: .8f);
    public Indicator KickDrum { get; private set; } = new Indicator(bouncerSpeed: .8f);
    public Indicator NoveltyEnergy { get; private set; } = new Indicator(bouncerSpeed: .8f, scale: 10f);
    public Indicator NoveltySpectral { get; private set; } = new Indicator(bouncerSpeed: .8f, scale: 2.5f);
    public Indicator ZeroCrossingRate { get; private set; } = new Indicator(bouncerSpeed: .8f, scale: 20f);
    Indicator[] indicators;

    void Start()
    {
        Instance = Instance ?? this;

        indicators = new Indicator[]
        {
            Volume,
            SnareDrum,
            HiHat,
            KickDrum,
            NoveltyEnergy,
            NoveltySpectral,
            ZeroCrossingRate,
        };

        if (udp)
        {
            udpThread = new Thread(new ThreadStart(UDPThreadMethod));
            udpThread.Start();
        }
        else
        {
            pythonScriptPath = ConvertToAbsoluteWinPath(pythonScriptPath);
            virtualEnvironmentPath = ConvertToAbsoluteWinPath(virtualEnvironmentPath);

            string command = "";
            command += $@"{virtualEnvironmentPath}Scripts\activate.bat";
            command += $@" & python {pythonScriptPath}";
            CommandPrompt.CommandPromptDelegate processFunction = ProcessData;
            process = commandPrompt.ExecuteCommand(command, processFunction);
        }
    }

    private void Update()
    {
        if (udp)
        {
            if (udpPrecessData)
            {
                /*lock object to make sure there data is 
                 *not being accessed from multiple threads at thesame time*/
                lock (udpLockObject)
                {
                    udpPrecessData = false;
                    ProcessData(udpMessage);

                    //Reset it for next read(OPTIONAL)
                    udpMessage = "";
                }
            }
        }
        else
            ProcessData(commandPrompt.PopOutput());
    }

    private void UDPThreadMethod()
    {
        udpClient = new UdpClient(5005);
        while (true)
        {
            IPEndPoint RemoteIpEndPoint = new IPEndPoint(IPAddress.Any, 0);

            byte[] receiveBytes = udpClient.Receive(ref RemoteIpEndPoint);

            /*lock object to make sure there data is 
            *not being accessed from multiple threads at thesame time*/
            lock (udpLockObject)
            {
                udpMessage = Encoding.ASCII.GetString(receiveBytes);

                //Done, notify the Update function
                udpPrecessData = true;
            }
        }
    }

    void ProcessData(string data)
    {
        if (data == null || data.Length < 6)
            return;

        int i = 0;
        foreach (string s in data.Split(','))
        {
            float value = 0;
            float.TryParse(
                s,
                System.Globalization.NumberStyles.Float,
                System.Globalization.CultureInfo.CreateSpecificCulture("en-GB"),
                out value
            );
            indicators[i].Value = value;
            i++;
        }
        VolumeSoft.Value = Volume.Value;
        AudioDisplay.Instance.DisplayAll(
            Volume,
            VolumeSoft,
            NoveltyEnergy,
            NoveltySpectral,
            ZeroCrossingRate,
            KickDrum, SnareDrum, HiHat
        );
    }

    string ConvertToRelativeWinPath(string path)
    {
        path = path.Replace(@"/", @"\");
        return path;
    }

    string ConvertToAbsoluteWinPath(string path)
    {
        path = Application.dataPath + path;
        path = ConvertToRelativeWinPath(path);
        return path;
    }

    private void OnApplicationQuit()
    {
        UnityEngine.Debug.Log("Close");
        process?.Close();
        udpThread?.Abort();
    }
}
