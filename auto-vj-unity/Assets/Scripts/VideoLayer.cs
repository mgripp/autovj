﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VideoLayer : MonoBehaviour {
	[SerializeField]
	public Renderer renderer;
	public bool active = false;
	
	Vector3 originalPos;

	// Use this for initialization
	void Start () {
		originalPos = transform.localPosition;
	}

	public void StartShaking(float magnitude, float[] samples, int i) {
		if (active)
			return;
		active = true;
		StartCoroutine (shakeLoop (magnitude, samples, i));
	}

	public void StopShaking() {
		if (!active)
			return;
		active = false;
		StopCoroutine ("shaking");
	}

	IEnumerator shakeLoop(float magnitude, float[] samples, int i) {
		
		while (active) {
			
			transform.localPosition = originalPos;
			StartCoroutine (shaking (1f, magnitude, samples, i));
			yield return new WaitForSeconds (1f);
		}

		yield return null;
	}

	IEnumerator shaking(float duration, float f, float[] samples, int i) {

		float elapsed = 0.0f;

		while (elapsed < duration && active) {     
			
			// map value to [-1, 1]
			float x = Random.value * 2.0f - 1.0f;
			float y = Random.value * 2.0f - 1.0f;
			x *= samples[i]*f;
			y *= samples[i]*f;

			transform.localPosition = originalPos + new Vector3(x, y, 0);

			elapsed += Time.deltaTime;

			yield return null;
		}

		transform.localPosition = originalPos;
	}
}
