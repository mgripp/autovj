using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEngine.Video;
using System.IO;

public class Playlist
{
    List<VideoClipWrapper> clips = new List<VideoClipWrapper>();
    string name;
    bool loop;
    bool sound;
    int shortcutIndex;

    public Playlist(
        string[] paths,
        string name,
        bool fullLength = false,
        bool loop = true,
        bool sound = false,
        int shortcutIndex = -1
    )
    {
        this.name = name;
        this.loop = loop;
        this.sound = sound;
        this.shortcutIndex = shortcutIndex;

        AddClips(paths);
    }

    public void AddClips(string[] paths)
    {
        foreach (string path in paths)
        {
            if (!File.Exists(path))
            {
                Debug.LogError($"File not found: {path}");
                continue;
            }

            string fileName;
            string folder;

            SplitURL(path, out folder, out fileName);
            // strip ending (.mp4)
            string ending;
            fileName = StripFileEnding(fileName, out ending);
            // ignore reverse clips
            if (fileName.EndsWith("REV"))
                continue;

            // try to find reverse clip to newly found clip
            string urlReverse = null;
            string pathReverse = folder + fileName + "REV" + ending;
            if (!File.Exists(pathReverse))
                Debug.Log($"No reverse clip found with {pathReverse}");
            else
                urlReverse = pathToURL(pathReverse);

            // add found Videoclip to playlist
            string url = pathToURL(path);
            Debug.Log($"Added {url} with reverse clip {urlReverse}");
            VideoClipWrapper clipWrapper = new VideoClipWrapper(url, urlReverse);
            clips.Add(clipWrapper);
        }
    }

    string pathToURL(string path) => $"file://{path}";

    public void AddClips(Playlist otherPlaylist)
    {
        foreach(VideoClipWrapper c in otherPlaylist.clips) {
            Debug.Log(c);
        }
        clips.AddRange(otherPlaylist.clips);
    }

    public void AddClips(List<VideoClipWrapper> newClips)
    {
        clips.AddRange(newClips);
    }

    void SplitURL(string url, out string folder, out string file)
    {
        string[] split = url.Split('/');
        file = split[split.Length - 1];
        folder = "";
        int i = split.Length - 1;
        foreach (string s in split)
        {
            if (i <= 0)
                break;
            folder += s + "/";
            i--;
        }
    }

    string StripFileEnding(string file, out string ending)
    {
        string fileName = file.Substring(0, file.Length - 4);
        ending = file.Substring(file.Length - 4);
        return fileName;
    }

    public int Count()
    {
        return clips.Count;
    }

    public bool Empty()
    {
        return Count() <= 0;
    }

    public VideoClipWrapper VideoClipAtIndex(int index)
    {
        return clips[index];
    }

    public VideoClipWrapper RandomClip()
    {
        int clipIndex = Random.Range(0, Count());
        return clips.ToArray()[clipIndex];
    }

    public void Print()
    {
        foreach (VideoClipWrapper clip in clips)
            Debug.Log(clip);
    }
}
