﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Flasher : Sequencer
{
    public Flasher(int lifetime) : base(lifetime)
    {
        actions = new Actions.Action[]
        {
            Actions.CallRandomAction,
            Actions.Flash,
            Actions.CallRandomAction,
            Actions.Flash,
            Actions.CallRandomAction,
            Actions.Flash,
            Actions.CallRandomAction,
            Actions.Flash,
            Actions.CallRandomAction,
            Actions.Flash,
        };
    }
}

