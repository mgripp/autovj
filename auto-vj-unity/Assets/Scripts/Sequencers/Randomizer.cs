﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Randomizer : Sequencer
{
    public Randomizer(int lifetime) : base(lifetime)
    {
        actions = new Actions.Action[]
        {
            Actions.CallRandomAction,
            Actions.CallRandomAction,
            Actions.CallRandomAction,
            Actions.CallRandomAction,
            Actions.CallRandomAction,
            Actions.CallRandomAction,
            Actions.CallRandomAction,
            Actions.CallRandomAction,
        };
    }
}
