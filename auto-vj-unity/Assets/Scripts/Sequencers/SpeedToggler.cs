﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpeedToggler : Sequencer
{
    public SpeedToggler(int lifetime) : base(lifetime)
    {
        actions = new Actions.Action[]
        {
            Actions.SwitchToNextMainPlayer,
            Actions.TogglePlaybackSpeed,
            Actions.TogglePlaybackSpeed,
            Actions.TogglePlaybackSpeed,
            Actions.TogglePlaybackSpeed,
            Actions.TogglePlaybackSpeed,
            Actions.TogglePlaybackSpeed,
            Actions.TogglePlaybackSpeed,
            Actions.TogglePlaybackSpeed,
        };
    }
}
