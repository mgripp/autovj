﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class Sequencer
{
    public static Func<int, Sequencer>[] classes = new Func<int, Sequencer>[]
    {
        lifetime => new SpeedToggler(lifetime),
        lifetime => new Flasher(lifetime),
        lifetime => new ReverseToggler(lifetime),
        lifetime => new PlayerSwitcher(lifetime),
        lifetime => new Randomizer(lifetime),
    };
    public static Sequencer GetRandomSequencer(int lifetime) => classes[UnityEngine.Random.Range(0, classes.Length)](lifetime);
    public static Sequencer GetRandomSequencer() => GetRandomSequencer(UnityEngine.Random.Range(4, 16));


    protected Actions.Action[] actions;
    int index = 0;
    int lifetime = 1;

    public Sequencer(int lifetime)
    {
        this.lifetime = lifetime;
    }

    public bool CallNextAction()
    {
        actions[index]();
        index = (index + 1) % actions.Length;
        if (index == 0)
        {
            lifetime--;
            if (lifetime < 1)
                return true;
        }

        return false;
    }
}
