﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerSwitcher : Sequencer
{
    public PlayerSwitcher(int lifetime) : base(lifetime)
    {
        actions = new Actions.Action[]
        {
            Actions.SwitchToNextMainPlayer,
            Actions.SwitchToNextMainPlayer,
            Actions.SwitchToNextMainPlayer,
            Actions.SwitchToNextMainPlayer,
            Actions.SwitchToNextMainPlayer,
            Actions.SwitchToNextMainPlayer,
            Actions.SwitchToNextMainPlayer,
            Actions.SwitchToNextMainPlayer,
        };
    }
}
