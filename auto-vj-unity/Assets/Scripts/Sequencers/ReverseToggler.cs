﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReverseToggler : Sequencer
{
    public ReverseToggler(int lifetime) : base(lifetime)
    {
        actions = new Actions.Action[]
        {
            Actions.SwitchToNextMainPlayer,
            Actions.ToggleReverse,
            Actions.ToggleReverse,
            Actions.ToggleReverse,
            Actions.ToggleReverse,
            Actions.ToggleReverse,
            Actions.ToggleReverse,
            Actions.ToggleReverse,
            Actions.ToggleReverse,
        };
    }
}
