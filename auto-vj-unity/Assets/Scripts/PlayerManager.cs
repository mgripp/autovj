﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerManager : MonoBehaviour
{
    public static PlayerManager Instance;

    [SerializeField] public VideoLayer mainLayer;
    [SerializeField] public VideoLayer alphaLayer;
    [SerializeField] public VideoLayer mainFadeLayer;
    [SerializeField] public VideoLayer alphaFadeLayer;
    [SerializeField] public VideoLayer blackFadeLayer;
    [SerializeField] public VideoLayer whiteFadeLayer;
    [SerializeField] bool autoEffectModeChange = true;
    [SerializeField] bool disableActions = false;
    [SerializeField] bool randomPlaylistChoose = false;
    [SerializeField] Panel panel;
    [SerializeField] UDPSend udpLightsSend;
    [SerializeField] UDPReceive udpMidi;


    bool actionLocked = false;
    bool betweenSongs = false;
    bool resetOnCooldown = false;
    bool resetting = false;

    public float actionCounterDelay = 0.05f;    // Zeit in s, die Beat-Aktion früher ausgeführt wird als eigentlich nach Beat-Abstandzeit richtig wäre. Dient dem Ausgleich von Verzögerung durch z.B. langsamen Rechner
    public float actionMinDelay = 0.1f;

    List<VideoPlayerWrapper> players = new List<VideoPlayerWrapper>();
    Playlist activePlaylist;

    int activePlayerNumber = -1;
    VideoPlayerWrapper activePlayer;
    // TODO
    int flashc = 0;
    bool flashb = true;


    void Start()
    {
        Application.runInBackground = true;

        if (Instance == null)
            Instance = this;

        alphaLayer.renderer.enabled = false;
        mainFadeLayer.renderer.enabled = false;
        alphaFadeLayer.renderer.enabled = false;
        blackFadeLayer.renderer.enabled = false;
        whiteFadeLayer.renderer.enabled = false;

        //Playlist.CreatePlaylists();
        
        panel.InitPlaylistDropdown(Settings.Instance.startPlaylist);
        print("+*****************************************");
        activePlaylist = PlaylistManager.Instance.Playlists[Settings.Instance.startPlaylist];
        LoadPlaylist(Settings.Instance.startPlaylist);

        ActionCooldown(5f);
    }

    void Update()
    {
        // UpdateIntensity ();

        // actionAmpReached = amp >= actionMinAmp;

		// if (!betweenSongs && amp < songChangeAmp) {
		// 	betweenSongs = true;
		// } else if(betweenSongs && amp >= songChangeAmp) {
		// 	betweenSongs = false;
		// 	// Reset (true);
		// }

        // if(amp < .2f)
        //     return;

        // if(Settings.Instance.useMidiBeat)
        // {
        //     //get midi beat from analyzer
        //     string udp = udpMidi.GetLatestUDPPacketString();
        //     print(udp);
        // } 
        // else
        // {
        //     if (hits > hitsBeat) {
        //         beat = true;
        //     }
        // }

        string lightsMessage = "0";

        // if(beat && flashb)
        // {
        //     Beat();
        //     lightsMessage += "beat";
        //     ControlLights(lightsMessage);

        //     if (flashc > 0) {
        //         StartCoroutine(Flash(averageBeatTime/2f));
        //         flashc --;
        //     }
        // }
    }

    public bool ActiveVideoPlayerHasReverse() => activePlayer.HasReverse;

    public void JumpToRandomTime()
    {
        if (resetting || activePlayer == null)
            return;
        activePlayer.JumpToRandomTime();
    }

    public void ToggleReverse()
    {
        if (resetting || activePlayer == null)
            return;
        if(!activePlayer.ToggleReverse())
            activePlayer.TogglePlaybackSpeed();
    }

    public void TogglePlaybackSpeed()
    {
        if (resetting || activePlayer == null)
            return;
        activePlayer.TogglePlaybackSpeed();
    }

    public void SetPlaybackSpeed(float speed)
    {
        if (resetting || activePlayer == null)
            return;
        activePlayer.SetPlaybackSpeed(speed);
    }

    public void SetPlaybackSpeedRandom()
    {
        if (resetting || activePlayer == null)
            return;
        activePlayer.SetPlaybackSpeedRandom();
    }

    public void Skip(float minTime, float maxTime)
    {
        if (resetting || activePlayer == null)
            return;
        activePlayer.Skip(minTime, maxTime);
    }

    public void ToggleShaking()
    {
        if (resetting || activePlayer == null)
            return;
        activePlayer.ToggleShaking();
    }

    public void StartShaking()
    {
        if (resetting || activePlayer == null)
            return;
        activePlayer.StartShaking();
    }

    public void StopShaking()
    {
        if (resetting || activePlayer == null)
            return;
        activePlayer.StopShaking();
    }

    public void ChangeClipRandom()
    {
        if (resetting || activePlayer == null)
            return;
        ChangeClipRandom(activePlayer);
        activePlayer.Play();
    }

    /**
    * Changes the clip of the VideoPlayerWrapper before the active one in the list of players randomly,
    * thus in background while another player is playing. 
    **/
    // TODO: With Change Interval of 1s breaks Unity Media Player (Restart needed), unsafe to use!
    public void ChangeClipInBackground()
    {
        Debug.Log("ChangeClipInBackground");

        int index = activePlayerNumber - 1;
        if (index < 0)
            index = players.Count - 1;
        ChangeClipRandom(players[index]);
    }

    public void ChangeClipRandom(VideoPlayerWrapper videoPlayer)
    {
        videoPlayer.Pause();
        videoPlayer.ChangeClip(activePlaylist.RandomClip());
        videoPlayer.Randomize();
    }

    public void Reset()
    {
        resetting = true;
        StartCoroutine(ReserRoutine());
    }

    IEnumerator ReserRoutine()
    {
        RemovePlayers();

        while(GetComponents<VideoPlayerWrapper>().Length > 0)
        {
            yield return null;
        }

        // Create players and fill them with randomly selected clips
        for (int i = 0; i < Settings.Instance.playersNumber; i++)
        {
            VideoPlayerWrapper player = AddPlayer(activePlaylist.RandomClip());
            player.Randomize();
        }
        // Check if players have been created
        if (players.Count < 1)
            Debug.LogError("No players created, clips missing?");

        SwitchToRandomPlayer();

        resetting = false;

        yield return new WaitForSeconds(.1f);

        SwitchToNextPlayer();
    }

    void ControlLights(string message)
    {
        if (!Settings.Instance.controlLightsEnabled)
            return;
        udpLightsSend.sendString(message);
    }

    // TODO
    IEnumerator Flash(float t)
    {
        yield return new WaitForSeconds(t);
        ControlLights("beat");
    }

    VideoPlayerWrapper AddPlayer(VideoClipWrapper videoClipWrapper)
    {
        VideoPlayerWrapper videoPlayer = gameObject.AddComponent<VideoPlayerWrapper>();
        videoPlayer.ChangeClip(videoClipWrapper);
        players.Add(videoPlayer);
        return videoPlayer;
    }

    public void RemovePlayers()
    {
        foreach (VideoPlayerWrapper player in players)
        {
            Destroy(player);
        }

        players = new List<VideoPlayerWrapper>();
    }

    public void LoadPlaylist(int index) => LoadPlaylist(PlaylistManager.Instance.PlaylistsAsArray[index]);
    public void LoadPlaylist(string name) => LoadPlaylist(PlaylistManager.Instance.Playlists[name]);
    void LoadPlaylistRandom() => LoadPlaylist(Random.Range(0, PlaylistManager.Instance.Playlists.Count));

    public void LoadPlaylist(Playlist playlist)
    {
        activePlaylist = playlist;
        if (activePlaylist.Empty())
            Debug.LogError("loaded playlist \"" + name + "\"is empty");
        activePlaylist.Print();
        Reset();
    }

    public void SwitchToNextPlayer()
    {
        int index = (activePlayerNumber + 1) % players.Count;
        SwitchToPlayer(index);
    }

    public void SwitchToRandomPlayer()
    {
        int index = Random.Range(0, players.Count);
        SwitchToPlayer(index);
    }

    public void SwitchToPlayer(int index)
    {
        if (resetting)
            return;
        if (activePlayerNumber > -1)
            activePlayer.Deactivate();
        activePlayerNumber = index;
        activePlayer = players[activePlayerNumber];
        activePlayer.Activate();
    }

    IEnumerator ActionCooldown(float time)
    {
        actionLocked = true;
        yield return new WaitForSeconds(time);
        actionLocked = false;
    }

    void RandomizePlayer()
    {
        SwitchToRandomPlayer();
        activePlayer.Randomize();
    }
}
