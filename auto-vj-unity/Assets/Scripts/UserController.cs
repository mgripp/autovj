﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UserController : MonoBehaviour
{
    string consoleInput = "";
    bool consoleOpen = false;

    void Update()
    {
        GetKeyboardInput();
        CheckConsoleInput();
    }

    // TODO: print Actions from Manager
    void GetKeyboardInput()
    {
        if (Input.GetKeyDown(KeyCode.D))
            VideoPlayerManager.Instance.SwitchToNextMainPlayer();
        if (Input.GetKeyDown(KeyCode.R))
            VideoPlayerManager.Instance.ToggleReverse();
        if (Input.GetKeyDown(KeyCode.J))
            VideoPlayerManager.Instance.JumpToRandomTime();
        if (Input.GetKeyDown(KeyCode.S))
            VideoPlayerManager.Instance.ToggleShaking();
        if (Input.GetKeyDown(KeyCode.H))
            DecisionMaker.Instance.AddRemoveRandomHitIndicator();
        if (Input.GetKeyDown(KeyCode.F))
            VideoPlayerManager.Instance.Flash();
        if (Input.GetKeyDown(KeyCode.Escape))
            Application.Quit();
        if (Input.GetKeyDown(KeyCode.Return))
            UnityEngine.SceneManagement.SceneManager.LoadScene("Main");

        // else if (Input.GetKeyDown(KeyCode.Alpha0))
        // {
        //     PlayerManager.Instance.ChangeClipRandom();
        //     Debug.Log("ChangeClipRandom");
        // }
        // else if (Input.GetKeyDown(KeyCode.Alpha1))
        // {
        //     PlayerManager.Instance.SwitchToNextPlayer();
        //     // Debug.Log("Switching to player: #");
        // }
        // else if (Input.GetKeyDown(KeyCode.Alpha2))
        // {
        //     PlayerManager.Instance.JumpToRandomTime();
        //     // Debug.Log("Jumped to random time: " + time);
        // }
        // else if (Input.GetKeyDown(KeyCode.Alpha3))
        // {
        //     PlayerManager.Instance.ToggleReverse();
        //     // Debug.Log("Running reverse: " + reverse);
        // }
        // else if (Input.GetKeyDown(KeyCode.Alpha4))
        // {
        //     PlayerManager.Instance.SetPlaybackSpeedRandom();
        //     // Debug.Log("Set random playback speet to: " + speed);
        // }
        // else if (Input.GetKeyDown(KeyCode.Alpha5))
        // {
        //     PlayerManager.Instance.Skip(-1f, 2f);
        //     // Debug.Log("Skipped " + skipTime + "s");
        // }
        // else if (Input.GetKeyDown(KeyCode.Alpha6))
        // {
        //     PlayerManager.Instance.ToggleShaking();
        //     // Debug.Log("Toggled shaking :" + shaking);
        // }
    }

    void ToggleConsole()
    {
        if (consoleOpen)
            CloseConsole();
        else
            OpenConsole();
    }

    void OpenConsole()
    {
        Debug.Log("Console opened");
        consoleOpen = true;
    }

    void CloseConsole()
    {
        consoleOpen = false;
        if (consoleInput.Length > 0)
        {
            int enteredNumber = int.Parse(consoleInput);
            Debug.Log("Loading Sortcut:" + enteredNumber);
            Playlist playlist = PlaylistManager.Instance.Shortcuts[enteredNumber];
            PlayerManager.Instance.LoadPlaylist(playlist);
            consoleInput = "";
        }
        Debug.Log("Console closed");
    }

    void CheckConsoleInput()
    {
        char enteredCharacter = GetKeyDownNumber();

        if (consoleOpen && enteredCharacter != '_' && consoleInput.Length < 2)
        {
            consoleInput += enteredCharacter;
            Debug.Log("consoleInput: " + consoleInput);
        }
    }

    char GetKeyDownNumber() {
        if (Input.GetKeyDown(KeyCode.Alpha0))
            return '0';
        if (Input.GetKeyDown(KeyCode.Alpha1))
            return '1';
        if (Input.GetKeyDown(KeyCode.Alpha2))
            return '2';
        if (Input.GetKeyDown(KeyCode.Alpha3))
            return '3';
        if (Input.GetKeyDown(KeyCode.Alpha4))
            return '4';
        if (Input.GetKeyDown(KeyCode.Alpha5))
            return '5';
        if (Input.GetKeyDown(KeyCode.Alpha6))
            return '6';
        if (Input.GetKeyDown(KeyCode.Alpha7))
            return '7';
        if (Input.GetKeyDown(KeyCode.Alpha8))
            return '8';
        if (Input.GetKeyDown(KeyCode.Alpha9))
            return '9';
        return '_';
    }
}
