﻿using System.Linq;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlaylistManager : MonoBehaviour
{
    static public PlaylistManager Instance;

    public Playlist[] PlaylistsAsArray
    {
        get
        {
            Playlist[] array = new Playlist[playlists.Values.Count];
            playlists.Values.CopyTo(array, 0);
            return array;
        }
    }
    public IReadOnlyDictionary<string, Playlist> Playlists => playlists;
    public IReadOnlyDictionary<int, Playlist> Shortcuts => shortcuts;

    Dictionary<string, Playlist> playlists = new Dictionary<string, Playlist>();
    Dictionary<int, Playlist> shortcuts = new Dictionary<int, Playlist>();

    private void Awake() => Instance = Instance ?? this;

    void Start()
    {
        // Get all playlist files
        FileInfo[] files = new DirectoryInfo(Settings.Instance.videoURL).GetFiles();
        foreach (FileInfo file in files)
            if (file.Extension == ".m3u")
                LoadStaticPlaylists(file.FullName, file.Name.Replace(file.Extension, string.Empty));
    }

    string RemoveHiddenCharacters(string path) => new string(path.Where(c => !char.IsControl(c)).ToArray());

    void LoadStaticPlaylists(string url, string name)
    {
        StreamReader reader = new StreamReader(url);
        string m3u = reader.ReadToEnd();
        string[] lines = m3u.Split('\n');
        List<string> paths = new List<string>();
        foreach (string line in lines)
        {
            if (!line.StartsWith("#") && line.Length > 0)
            {
                // Remove all hidden characters from path
                string path = RemoveHiddenCharacters(line);
                if (!path.EndsWith(".mp4") && Directory.Exists(path))
                {
                    foreach (string file in Directory.GetFiles(path))
                        paths.Add(file);
                }
                else
                    paths.Add(path);
            }
        }

        playlists.Add(name, new Playlist(
            paths: paths.ToArray(),
            name: name
        ));

        Debug.Log($"Loaded playlist '{name}' from {url}");
    }
}
