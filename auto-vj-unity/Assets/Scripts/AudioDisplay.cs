﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioDisplay : MonoBehaviour
{
    public static AudioDisplay Instance { get; private set; }

    [SerializeField] RectTransform rectTransform;
    [SerializeField] Bar kdBar;
    [SerializeField] Bar sdBar;
    [SerializeField] Bar hhBar;
    [SerializeField] Bar noveltyEnergyBar;
    [SerializeField] Bar noveltySpectralBar;
    [SerializeField] Bar zeroCrossingRateBar;
    [SerializeField] Bar volumeBar;
    [SerializeField] Bar volumeBarSoft;
    [SerializeField] HitToggle[] hitToggles;
    [SerializeField] GameObject beatDisplay;
    [SerializeField] GameObject barPrefab;
    // TODO put in settings
    //[SerializeField] public int averageRange {
    //    set { SetFrequencySpectrumDisplayAverageRange(value); }
    //    get { return frequencySpectrumDisplay[0].AverageRange; }
    //}

    List<Bar> frequencySpectrumDisplay = new List<Bar>();

    private void Awake() => Instance = Instance ?? this;

    private void Start()
    {
        foreach (HitToggle h in hitToggles)
            h.enabled = true;
    }

    public void Inititalize(int bandwidth)
    {
        for (int i = 0; i < bandwidth; i++)
        {
            Bar bar = GameObject.Instantiate(barPrefab, transform).GetComponent<Bar>();
            frequencySpectrumDisplay.Add(bar);
            float width = rectTransform.rect.width * 0.9f;
            bar.transform.localPosition = new Vector3(-width / 2f + width * ((float)i / (float)bandwidth), -.5f * rectTransform.rect.height, 0);
        }
        SetMax(Settings.Instance.frequencyBandMaxAmplitude);
    }

    public void SetMax(float max)
    {
        foreach (Bar bar in frequencySpectrumDisplay)
            bar.max = max;
    }

    public void DisplayFequencySpectrum(Indicator indicator, int index)
    {
        frequencySpectrumDisplay[index].Display(indicator);
    }

    public void DisplayAll(
        Indicator volume,
        Indicator volumeSoft,
        Indicator noveltyEnergy,
        Indicator noveltySpectral,
        Indicator zeroCrossingRate,
        Indicator kickDrum,
        Indicator snareDrum,
        Indicator HiHat
    )
    {
        noveltyEnergyBar.Display(noveltyEnergy);
        noveltySpectralBar.Display(noveltySpectral);
        zeroCrossingRateBar.Display(zeroCrossingRate);
        kdBar.Display(kickDrum);
        sdBar.Display(snareDrum);
        hhBar.Display(HiHat);
        DisplayVolume(volume, volumeSoft);
    }

    public void DisplayVolume(Indicator indicator, Indicator indicatorSoft)
    {
        volumeBar.Display(indicator);
        volumeBarSoft.Display(indicatorSoft);
    }

    public void DisplayBeat()
    {
        StartCoroutine(DisplayBeatCoroutine());
    }

    IEnumerator DisplayBeatCoroutine()
    {
        beatDisplay.SetActive(true);
        yield return new WaitForSeconds(.05f);
        beatDisplay.SetActive(false);
    }
}
