﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HitToggle : MonoBehaviour
{
    static List<HitToggle> Instances = new List<HitToggle>();

    public static void UpdateInstances()
    {
        foreach (HitToggle h in Instances)
            h.toggle.SetIsOnWithoutNotify(DecisionMaker.Instance.UsesHitIndicator(h.indicator));
    }

    [SerializeField] Toggle toggle;
    Indicator indicator;

    private void Start()
    {
        Instances.Add(this);

        if (toggle == null)
            toggle = GetComponent<Toggle>();

        if (indicator == null)
        {
            indicator = transform.parent.GetComponent<Bar>().indicator;
            if (indicator != null)
                toggle.SetIsOnWithoutNotify(DecisionMaker.Instance.UsesHitIndicator(indicator));
        }
    }

    public void Toggle(bool value)
    {
        if (value)
        {
            print(indicator);
            DecisionMaker.Instance.AddHitIndicator(indicator);
        }
        else
            DecisionMaker.Instance.RemoveHitIndicator(indicator);
    }
}
