﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Bar : MonoBehaviour
{
    [SerializeField] RectTransform rectTransform;
    [SerializeField] RectTransform bouncerTransform;
    [SerializeField] TMP_Text title;
    [SerializeField] GameObject hitDisplay;
    [SerializeField] bool debug = false;
    [HideInInspector] public Indicator indicator;

    public float max = 1f;
    float height = 0;

    public void Display(Indicator indicator)
    {
        this.indicator = indicator;
        if (debug) print(indicator.Value);
        float newHeight = height * indicator.Value / max;
        rectTransform.sizeDelta = new Vector2(rectTransform.sizeDelta.x, newHeight);
        bouncerTransform.localPosition = 
            new Vector3(bouncerTransform.localPosition.x,
            -rectTransform.localPosition.y + height * indicator.BouncerValue / max,
            bouncerTransform.localPosition.z);
        hitDisplay.SetActive(indicator.Hit);
        if (title != null)
            title.text = gameObject.name.Replace(" Bar", "");
    }

    private void Awake()
    {
        height = rectTransform.rect.height;
    }
}
