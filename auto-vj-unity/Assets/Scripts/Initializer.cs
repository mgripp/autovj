﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Initializer : MonoBehaviour
{
    [SerializeField] MonoBehaviour[] scripts;

    private void Start()
    {
        foreach (MonoBehaviour script in scripts)
            script.enabled = true;
    }
}
