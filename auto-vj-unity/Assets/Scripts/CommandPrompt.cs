﻿using System.Diagnostics;
using System.Collections.Generic;
using UnityEngine;

public class CommandPrompt
{
    public delegate void CommandPromptDelegate(string data);
    Queue<string> outputBuffer = new Queue<string>();

    public string PopOutput()
    {
        if (outputBuffer.Count > 0)
            return outputBuffer.Dequeue();
        return null;
    }

    void PutOutput(string data) => outputBuffer.Enqueue(data);

    public Process ExecuteCommand(string command, CommandPromptDelegate processFunction)
    {
        UnityEngine.Debug.Log($"Executing command: {command}");
        command = @"/k " + @command;
        ProcessStartInfo processInfo = new ProcessStartInfo();
        processInfo.UseShellExecute = false;
        processInfo.CreateNoWindow = true;
        processInfo.ErrorDialog = true;
        processInfo.RedirectStandardOutput = true;
        processInfo.RedirectStandardError = true;
        processInfo.FileName = "cmd.exe";
        processInfo.Arguments = @command;

        Process process = new Process();
        process.StartInfo = processInfo;
        process.EnableRaisingEvents = true;
        process.OutputDataReceived += new DataReceivedEventHandler((sender, e) => PutOutput(e.Data));
        process.ErrorDataReceived += new DataReceivedEventHandler((sender, e) => UnityEngine.Debug.LogWarning(e.Data));
        process.Start();
        process.BeginOutputReadLine();
        process.BeginErrorReadLine();
        return process;
    }
}