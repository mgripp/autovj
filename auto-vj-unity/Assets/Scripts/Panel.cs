using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Panel : MonoBehaviour
{
    public static Panel Instance;

    [SerializeField] Canvas canvas;
    [SerializeField] Slider volumeSlider;
    [SerializeField] Slider hitsBeatSlider;
    [SerializeField] Slider ddSlider;
    [SerializeField] Text messageText;
    [SerializeField] Text ampText;
    [SerializeField] Text hitsBeatText;
    [SerializeField] Text bpmText;
    [SerializeField] Text rewardText;
    [SerializeField] Text ddText;
    [SerializeField] Dropdown playlistsDropdown;
    [SerializeField] Toggle hitOperatorToggle;

    float dropDelay = 0;

    void Start()
    {
        Instance = Instance ?? this;

        //volumeSlider.value = Settings.Instance.volume;
        SliderChangedDropDelay(ddSlider.value);
        SliderChangedHitBeats(hitsBeatSlider.value);
        hitOperatorToggle.isOn = DecisionMaker.Instance.hitOperatorOr;
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Tab))
            Toggle();
    }

    public void ToggleHitOperator(bool value) => DecisionMaker.Instance.hitOperatorOr = value;
    public void UpdateHitOperator() => hitOperatorToggle.SetIsOnWithoutNotify(DecisionMaker.Instance.hitOperatorOr);

    public void InitPlaylistDropdown(string startName)
    {
        InitPlaylistDropdown(PlaylistManager.Instance.Playlists, 0);
        foreach (Dropdown.OptionData option in playlistsDropdown.options)
        {
            if (option.text.Equals(startName))
            {
                playlistsDropdown.value = playlistsDropdown.options.IndexOf(option);
                break;
            }  
        }
    }

    public void InitPlaylistDropdown(IReadOnlyDictionary<string, Playlist> playlists, int startIndex)
    {
        List<Dropdown.OptionData> options = new List<Dropdown.OptionData>();
        foreach (string key in playlists.Keys)
        {
            Dropdown.OptionData option = new Dropdown.OptionData();
            option.text = key;
            options.Add(option);
        }
        playlistsDropdown.AddOptions(options);
        playlistsDropdown.value = startIndex;
        playlistsDropdown.RefreshShownValue();
    }

    public void SetPlaylist(int index)
    {
        string playlist = playlistsDropdown.options[index].text;
        print(index + " " + playlist);
        PlayerManager.Instance.LoadPlaylist(playlist);
    }

    void Toggle()
    {
        bool b = true;
        if (canvas.enabled)
        {
            b = false;
        }

        canvas.enabled = b;
    }

    public void DisplayReward(float reward)
    {
        rewardText.text = "Reward: " + reward;
    }

    public void SetUseStereomix(bool useStereoMix)
    {
        AudioAnalyzer.Instance.SetInput(useStereoMix);
    }

    public void ChangeVolume(float volume)
    {
        Settings.Instance.volume = volume;
        ampText.text = volume.ToString();
    }

    public void SliderChangedHitBeats(float value)
    {
        int iValue = (int)value;
        if (AudioAnalyzer.Instance == null)
            return;
        AudioAnalyzer.Instance.hitsBeat = iValue;
        hitsBeatText.text = iValue.ToString();
    }

    public void SliderChangedDropDelay(float value)
    {
        dropDelay = value;
        ddText.text = value.ToString();
    }

    public void SetMessage(string s)
    {
        messageText.text = s;
    }

    public void SetBPMText(float bpm)
    {
        bpmText.text = "BPM: " + bpm;
    }
}
