﻿using UnityEngine.Video;

public class VideoClipWrapper
{
    public string url;
    public string urlReverse;
    public bool HasReverse { get; private set; } = false;

    public VideoClipWrapper(string url, string urlReverse = null) {
        this.url = url;
        this.urlReverse = urlReverse;
        HasReverse = urlReverse != null;
    }

    override public string ToString() => $"url: {url}, has reverse: {HasReverse}";
}
