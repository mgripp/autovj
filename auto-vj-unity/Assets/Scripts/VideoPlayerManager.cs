﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VideoPlayerManager : MonoBehaviour
{
    static public VideoPlayerManager Instance;

    [SerializeField] GameObject playerPrefab;

    List<VideoPlayerWrapper> players = new List<VideoPlayerWrapper>();
    Playlist activePlaylist;

    int mainPlayerIndex = -1;
    VideoPlayerWrapper mainPlayer;

    private void Awake() => Instance = Instance ?? this;

    void Start()
    {
        for (int i = 0; i < Settings.Instance.videoCount; i++)
            AddPlayer();

        LoadPlaylist(Settings.Instance.startPlaylist);
        SwitchMainPlayerRandom();
    }

    public void LoadPlaylist(int index) => LoadPlaylist(PlaylistManager.Instance.PlaylistsAsArray[index]);
    void LoadPlaylistRandom() => LoadPlaylist(Random.Range(0, PlaylistManager.Instance.Playlists.Count));

    public void LoadPlaylist(string name)
    {
        try
        {
            LoadPlaylist(PlaylistManager.Instance.Playlists[name]);
        }
        catch
        {
            Debug.LogError($"Playlist '{name}' not found, loading first known playlist instead.");
            LoadPlaylist(0);
        }
    }

    public void LoadPlaylist(Playlist playlist)
    {
        if (playlist.Empty())
        {
            Debug.LogError($"Loaded playlist '{name}' is empty");
            return;
        }
        activePlaylist = playlist;
        activePlaylist.Print();

        foreach (VideoPlayerWrapper player in players)
        {
            player.ChangeClip(activePlaylist.RandomClip());
            player.RandomizeDelayed();
        }
    }

    void AddPlayer()
    {
        VideoPlayerWrapper player = Instantiate(playerPrefab, transform).GetComponent<VideoPlayerWrapper>();
        player.name = $"VideoPlayer {players.Count + 1}";
        players.Add(player);
    }

    /**
    * Changes the clip of the VideoPlayerWrapper before the active one in the list of players randomly,
    * thus in background while another player is playing. 
    **/
    public void ChangeClipInBackground()
    {
        Debug.Log("ChangeClipInBackground");

        int index = mainPlayerIndex - 1;
        if (index < 0)
            index = players.Count - 1;
        ChangeClipRandom(players[index]);
    }

    public void ChangeClipRandom(VideoPlayerWrapper videoPlayer)
    {
        videoPlayer.ChangeClip(activePlaylist.RandomClip());
        videoPlayer.RandomizeDelayed();
    }

    public void SwitchMainPlayer(int index)
    {
        if (mainPlayer != null)
            mainPlayer.Deactivate();
        index = index % players.Count;
        mainPlayer = players[index];
        mainPlayerIndex = index;
        mainPlayer.Activate();
        //Debug.Log($"Switched to main player #{mainPlayerIndex}: '{mainPlayer}'");
    }

    public void CalmAllPlayers()
    {
        print("Calm");
        foreach (VideoPlayerWrapper player in players)
            player.SetPlaybackSpeed(1f);
    }

    public void SwitchToNextMainPlayer() => SwitchMainPlayer(mainPlayerIndex + 1);
    public void SwitchMainPlayerRandom() => SwitchMainPlayer(Random.Range(0, players.Count));
    public void TogglePlaybackSpeed() => mainPlayer.TogglePlaybackSpeed();
    public void ToggleReverse() => mainPlayer.ToggleReverse();
    public void ToggleShaking() => mainPlayer.ToggleShaking();
    public void JumpToRandomTime() => mainPlayer.JumpToRandomTime();
    public void Flash() => mainPlayer.Flash();
}
