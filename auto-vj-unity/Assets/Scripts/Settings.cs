﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class Settings : MonoBehaviour
{
    static public Settings Instance { get; private set; }

    [SerializeField] public string lightsServerAdress = "192.168.0.158";
    [SerializeField] public bool controlLightsEnabled = true;
    [SerializeField] public bool useMidiBeat = true;

    [SerializeField] public bool shakeEnabled = true;
    [SerializeField] public bool changeClipsInBackground = true;
    [SerializeField] public float changeClipsInBackgroundInterval = 30;
    [SerializeField] public int changeClipBeatcount = 4;
    [SerializeField] public string startPlaylist = "anime";
    [SerializeField] public int playersNumber = 4;
    [SerializeField] public float frequencyBandMaxAmplitude = 10f;
    [SerializeField] public float bouncerDescentSpeed = 1f;
    [SerializeField] public float indicatorHitCooldown = 0.01f;
    [SerializeField] public float indicatorHitMinValue= 0.1f;
    [SerializeField] public int AverageRange = 10;
    [SerializeField] public float BeatHits = .5f;
    [SerializeField] public float beatCooldown = .2f;
    [SerializeField] public float sampleMaxValue = .0007f;
    [SerializeField] public float clipChangeInterval = 180f;


    [SerializeField] public float brightnessScale = 1f;
    [SerializeField] public float contrastScale = 1f;
    [SerializeField] public float colorScale = 1f;
    [SerializeField] public float flashDuration = .2f;

    [SerializeField] public AnimationCurve volumeCurve;
    [SerializeField] public float volume = 1f;
    [SerializeField] public float minVolume = .1f;

    [SerializeField] public float brightness = 1f;

    [SerializeField] public string videoURL = "Undefined";
    [SerializeField] public int videoCount = 2;

    private void Awake()
    {
        Instance = Instance ?? this;
        Application.runInBackground = true;
    }

    void Start()
    {
        if (!Directory.Exists(videoURL))
            videoURL = $"{GetApplicationPath()}/video";
        if (!Directory.Exists(videoURL))
            throw new FileNotFoundException($"Could not load video clips from {videoURL} since it does not exist.");
    }

    // https://answers.unity.com/questions/13072/how-do-i-get-the-application-path.html
    string GetApplicationPath()
    {
        string path = Application.dataPath;
        if (Application.platform == RuntimePlatform.OSXPlayer)
            path += "/../../";
        else if (Application.platform == RuntimePlatform.WindowsPlayer)
            path += "/../";
        return path;
    }
}
