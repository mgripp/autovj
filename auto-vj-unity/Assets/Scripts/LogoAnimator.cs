﻿using System;
using System.Collections;
using UnityEngine;
using Random = UnityEngine.Random;

public class LogoAnimator : MonoBehaviour
{
    static public LogoAnimator Instance;

    [SerializeField]
    GameObject raveYardBlack;
    [SerializeField]
    GameObject raveYardWhite;

    bool flipFlop = false;
    int mode = 0;
    float modeTime = 0;
    Coroutine animation = null;

    void Start()
    {
        Instance = Instance ?? this;
        NewActionMode();
    }

    private void Update()
    {
        modeTime += Time.deltaTime;
    }

    public void Action()
    {
        if (animation != null)
            return;

        switch (mode)
        {
            // Flip Flop
            case 0 :
                FlipFlop();
                break;
            // Flicker White
            case 1:
                animation = StartCoroutine(FlickerRoutineWhite());
                break;
            // Flicker Black
            case 2:
                animation = StartCoroutine(FlickerRoutineBlack());
                break;
            // Flash White
            case 3:
                animation = StartCoroutine(FlashRoutineWhite());
                break;
            // Flash Black
            case 4:
                animation = StartCoroutine(FlashRoutineBlack());
                break;
            default:
                Clear();
                break;
        }
    }

    void Clear()
    {
        SetActiveWhite(false);
        SetActiveBlack(false);
    }

    IEnumerator WaitForModeEnd(float time)
    {
        yield return new WaitForSeconds(time);
        NewActionMode();
    }

    IEnumerator FlashRoutineWhite()
    {
        SetActiveWhite(true);
        yield return new WaitForSeconds(.05f);
        SetActiveWhite(false);
        animation = null;
    }

    IEnumerator FlashRoutineBlack()
    {
        SetActiveBlack(true);
        yield return new WaitForSeconds(.05f);
        SetActiveBlack(false);
        animation = null;
    }

    IEnumerator FlickerRoutineWhite()
    {
        while(mode == 1)
        {
            SetActiveWhite(!raveYardWhite.activeSelf);
            yield return new WaitForSeconds(.04f);
        }
    }

    IEnumerator FlickerRoutineBlack()
    {
        while (mode == 2)
        {
            SetActiveBlack(!raveYardBlack.activeSelf);
            yield return new WaitForSeconds(.04f);
        }
    }

    void NewActionMode()
    {
        mode = Random.Range(0, 8);
        modeTime = 0;

        if (animation != null)
        {
            StopCoroutine(animation);
            animation = null;
        }

        StartCoroutine(WaitForModeEnd(Random.Range(1f, 7f)));
    }

    void FlipFlop()
    {
        flipFlop = !flipFlop;
        SetActiveWhite(flipFlop);
        SetActiveBlack(!flipFlop);
    }

    void SetActiveWhite(bool active)
    {
        raveYardWhite.SetActive(active);
        if (active && raveYardBlack.activeSelf)
            raveYardBlack.SetActive(false);
    }

    void SetActiveBlack(bool active)
    {
        raveYardBlack.SetActive(active);
        if (active && raveYardWhite.activeSelf)
            raveYardWhite.SetActive(false);
    }
}
