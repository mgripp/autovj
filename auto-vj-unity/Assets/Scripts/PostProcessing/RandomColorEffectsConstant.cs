﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomColorEffectsConstant : PostProcessAnimator
{
    float amount = 0;
    Vector3 rgb = new Vector3();

    public RandomColorEffectsConstant() : base() {
        amount = Random.Range(0, 1f);
        rgb = new Vector3(Random.Range(0, 100f), Random.Range(0, 100f), Random.Range(0, 100f)) * Random.Range(.2f, 1f);

        colorGrading.contrast.value = 20f;
        colorGrading.brightness.value = 20f;
        colorGrading.mixerRedOutRedIn.value = rgb.x;
        colorGrading.mixerGreenOutGreenIn.value = rgb.y;
        colorGrading.mixerBlueOutBlueIn.value = rgb.z;
    }
}
