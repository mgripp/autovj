﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering.PostProcessing;

public class PostProcessingManager : MonoBehaviour
{
    public static PostProcessingManager Instance { get; private set; }
    private void Awake() => Instance = Instance ?? this;

    [SerializeField] public PostProcessProfile profile;
}
