﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering.PostProcessing;

public abstract class PostProcessAnimator
{
    static protected Indicator ZeroIndicator = new Indicator();
    static PostProcessAnimator() => ZeroIndicator.Value = -100f;

    protected PostProcessProfile profile => PostProcessingManager.Instance.profile;
    protected ColorGrading colorGrading;

    public PostProcessAnimator() => profile.TryGetSettings(out colorGrading);
    public bool flashing = false;

    virtual public void Tick()
    {
        if (flashing)
        {
            colorGrading.contrast.value = 100f;
            colorGrading.brightness.value = 100f;
            colorGrading.mixerRedOutRedIn.value = 100f;
            colorGrading.mixerGreenOutGreenIn.value = 100f;
            colorGrading.mixerBlueOutBlueIn.value = 100f;
        }

        colorGrading.brightness.value -= (1f - Settings.Instance.brightness) * 100f;
    }
}
