﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomColorEffectsDynamic : PostProcessAnimator
{
    Indicator redIndicator;
    Indicator greenIndicator;
    Indicator blueIndicator;
    Indicator contrastIndicator;
    Indicator brightnessIndicator;

    public RandomColorEffectsDynamic() : base() {
        Indicator[] indicators = new Indicator[]
        {
            Backend.Instance.Volume,
            Backend.Instance.NoveltyEnergy,
            Backend.Instance.NoveltySpectral,
            Backend.Instance.ZeroCrossingRate,
        };
        redIndicator = indicators[Random.Range(0, indicators.Length)];
        greenIndicator = indicators[Random.Range(0, indicators.Length)];
        blueIndicator = indicators[Random.Range(0, indicators.Length)];
        contrastIndicator = indicators[Random.Range(0, indicators.Length)];
        brightnessIndicator = indicators[Random.Range(0, indicators.Length)];
    }

    public override void Tick()
    {
        colorGrading.contrast.value = contrastIndicator.Value * Settings.Instance.contrastScale;
        colorGrading.brightness.value = brightnessIndicator.Value * Settings.Instance.brightnessScale;
        colorGrading.mixerRedOutRedIn.value = redIndicator.Value * Settings.Instance.colorScale;
        colorGrading.mixerGreenOutGreenIn.value = greenIndicator.Value * Settings.Instance.colorScale;
        colorGrading.mixerBlueOutBlueIn.value = blueIndicator.Value * Settings.Instance.colorScale;

        base.Tick();
    }
}
