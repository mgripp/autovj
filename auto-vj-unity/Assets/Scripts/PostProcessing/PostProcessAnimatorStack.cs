﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class PostProcessAnimatorStack : MonoBehaviour
{
    public static Func<PostProcessAnimator>[] classes = new Func<PostProcessAnimator>[]
    {
        () => new RandomColorEffectsConstant(),
        () => new RandomColorEffectsDynamic(),
    };
    public static PostProcessAnimator GetRandomAnimator() => classes[UnityEngine.Random.Range(0, classes.Length)]();

    List<PostProcessAnimator> animators = new List<PostProcessAnimator>();

    void Update()
    {
        foreach (PostProcessAnimator animator in animators)
            animator.Tick();
    }

    public void AddNewAnimator(PostProcessAnimator animator) => animators.Add(animator);
    public void AddRandomAnimator() => animators.Add(GetRandomAnimator());
    public void ClearAnimators() => animators = new List<PostProcessAnimator>();

    public void Flash() => StartCoroutine(FlashRoutine());

    IEnumerator FlashRoutine()
    {
        foreach (PostProcessAnimator a in animators)
            a.flashing = true;
        yield return new WaitForSeconds(Settings.Instance.flashDuration);
        foreach (PostProcessAnimator a in animators)
            a.flashing = false;
    }
}
