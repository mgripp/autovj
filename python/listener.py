import audio_analyze
import player_reader
import time

"""
You must run this script as admin or librosa will not be able to load und the program gets stuck during import.
"""

def main():
    audio_path = 'Rene LaVice - The Calling Feat. Ivy Mairi (152kbit_Opus).mp3'
    print("Analyzing {}...".format(audio_path))
    audio_analyzation = audio_analyze.analyze_track(audio_path)

    reader = player_reader.PlayerReader()

    while True:
        current_track_time = reader.current_track_time
        time_text = player_reader.get_time_as_text(seconds=current_track_time, seconds_fractured=False)

        beat_event, amplitude = audio_analyzation.get_audio_data(current_track_time)

        beat_text = ""
        if beat_event:
            beat_text = "BEAT"
        
        text = "{}\tAmp: {}\t{}".format(time_text, amplitude, beat_text)
        print(text)



if __name__ == "__main__":
    main()