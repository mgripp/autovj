"""
https://musicinformationretrieval.com/novelty_functions.html
"""

import numpy, scipy, matplotlib.pyplot as plt, IPython.display as ipd
import librosa, librosa.display

# get signal data
x, sr = librosa.load('audio/wav.wav')
# plt.figure(figsize=(14, 5))
# librosa.display.waveplot(x, sr)

# get root-mean-square energy
hop_length = 512
frame_length = 1024
rmse = librosa.feature.rms(x, frame_length=frame_length, hop_length=hop_length).flatten()
rmse_diff = numpy.zeros_like(rmse)
rmse_diff[1:] = numpy.diff(rmse)

energy_novelty = numpy.max([numpy.zeros_like(rmse_diff), rmse_diff], axis=0)
frames = numpy.arange(len(rmse))
t = librosa.frames_to_time(frames, sr=sr)

# plt.figure(figsize=(15, 6))
# plt.plot(t, rmse, 'b--', t, rmse_diff, 'g--^', t, energy_novelty, 'r-')
# plt.xlim(0, t.max())
# plt.xlabel('Time (sec)')
# plt.legend(('RMSE', 'delta RMSE', 'energy novelty')) 

# perform logarithmic compression
log_rmse = numpy.log1p(10*rmse)
log_rmse_diff = numpy.zeros_like(log_rmse)
log_rmse_diff[1:] = numpy.diff(log_rmse)
log_energy_novelty = numpy.max([numpy.zeros_like(log_rmse_diff), log_rmse_diff], axis=0)

plt.figure(figsize=(15, 6))
plt.plot(t, log_rmse, 'b--', t, log_rmse_diff, 'g--^', t, log_energy_novelty, 'r-')
plt.xlim(0, t.max())
plt.xlabel('Time (sec)')
plt.legend(('log RMSE', 'delta log RMSE', 'log energy novelty'))

plt.show()
