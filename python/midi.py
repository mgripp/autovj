import socket
import time
import mido
import threading

UDP_IP_RECEIVE = "0.0.0.0"
UDP_PORT_RECEIVE = 5005
UDP_IP_SEND = "127.0.0.1"
UDP_PORT_SEND = 5006

send_socket = socket.socket(socket.AF_INET, # Internet
    socket.SOCK_DGRAM) # UDP

receive_socket = socket.socket(socket.AF_INET, # Internet
    socket.SOCK_DGRAM) # UDP
receive_socket.bind((UDP_IP_RECEIVE, UDP_PORT_RECEIVE))

print(mido.get_input_names())

beat_signal_count = 0
send_after_beats = 3
midi_inputs = ['LoopBe Internal MIDI 0', 'LoopBe Internal MIDI 1']
midi_input_index = 0
midi_input = midi_inputs[midi_input_index]

def send():
    global beat_signal_count
    global send_after_beats
    global send_socket
    global midi_input_index
    global midi_input
    global midi_inputs

    try:
        with mido.open_input(midi_input) as inport:
            for msg in inport:
                msg = inport.receive()
                # print(time.time(), msg)
                beat_signal_count += 1
                if beat_signal_count % send_after_beats == 0:
                    beat_count = int((beat_signal_count % 24) / 3)
                    send_socket.sendto(bytes([beat_count]), (UDP_IP_SEND, UDP_PORT_SEND))
                    bar_reached = (beat_count == 0)
                    half_bar_reached = (beat_count == 4)
                    bar_reached_text = ""
                    half_bar_reached_text = ""
                    if bar_reached:
                        bar_reached_text = "BAR"
                    if bar_reached or half_bar_reached:
                        half_bar_reached_text = "HALF"
                    print("{}\tBEAT:\t{}\t{}\t{}".format(time.time(), beat_count, half_bar_reached_text, bar_reached_text))
    except:
        midi_input_index = (midi_input_index + 1) % len(midi_inputs)
        midi_input = midi_sources[midi_input_index]

def receive():
    global send_after_beats
    global receive_socket
    while True:
        data, addr = receive_socket.recvfrom(1024) # buffer size is 1024 bytes
        send_after_beats = int(str(data))
        print("Set send_after_beats to {}".format(send_after_beats))

def main():
    send()
    # send_thread = threading.Thread(target=send)
    # receive_thread = threading.Thread(target=receive)
    # send_thread.start()
    # receive_thread.start()

if __name__ == "__main__":
    main()