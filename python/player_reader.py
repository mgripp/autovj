from PIL import ImageGrab
import numpy as np
import cv2
import time
import pytesseract
import threading
pytesseract.pytesseract.tesseract_cmd = r'C:\\Users\\Marius\\AppData\\Local\\Tesseract-OCR\\tesseract.exe'

# (bbox = x, y, width, height)
time_a_bbox = (600, 215, 670, 235)
time_b_bbox = (1245, 215, 1320, 235)
active_a_bbox = (600, 215, 670, 235)
active_b_bbox = (600, 215, 670, 235)

def invert(self, image):
        image = (255 - image)
        return image

def get_screenshot(bbox):
        image = ImageGrab.grab(bbox=bbox)
        image_np = np.array(image)
        image = cv2.cvtColor(image_np, cv2.COLOR_RGB2GRAY)
        image_np = np.array(image)

        # cv2.imshow('final', image)
        # cv2.waitKey(0)

        return image

def get_time_text(time):
    image = get_screenshot(time)
    text = pytesseract.image_to_string(image, config="--psm 10 --oem 3 -c tessedit_char_whitelist=0123456789:.")
    return text

def text_to_int(text):
        try:
            i = int(text)
        except:
            i = 0
        return i

def hms_to_seconds(hours: int, minutes: int, seconds: float):
    return hours * 3600 + minutes * 60 + seconds

def seconds_to_hms(seconds):
    hours = int(np.floor(seconds / 3600))
    seconds -= hours * 3600
    minutes = int(np.floor(seconds / 60))
    seconds -= minutes * 60

    return (hours, minutes, seconds)

def get_time_seconds(bbox):
    time_start = time.time()

    text = get_time_text(bbox)
    text_parts = text.split(':')
    text_parts_len = len(text_parts)
    seconds_with_fracture = text_parts[text_parts_len - 1].split('.')
    seconds_with_fracture_len = len(seconds_with_fracture)

    # no valid time data extracted: minutes and seconds at least must be visible
    if seconds_with_fracture_len < 1 or text_parts_len < 2:
        return -1
    
    # get milliseconds (might not be present)
    milliseconds = ""
    milliseconds_present = False
    if len(seconds_with_fracture) == 2:
        milliseconds = seconds_with_fracture[1]
        milliseconds_present = True


    # get seconds
    seconds = seconds_with_fracture[0]

    # get minutes
    minutes = text_parts[text_parts_len - 2]

    # get hours: only visible when milliseconds are not
    hours = ""
    if not milliseconds_present:
        hours = text_parts[0]
    
    # remove lag
    lag_time = time.time() - time_start
    # print(lag_time)

    # convert to numbers
    hours_int = text_to_int(hours)
    minutes_int = text_to_int(minutes)
    seconds_int = text_to_int(seconds)
    milliseconds_int = text_to_int(milliseconds)
    seconds_float = seconds_int + float(milliseconds_int) / 10 + lag_time

    # sum sup
    seconds_total = hms_to_seconds(hours_int, minutes_int, seconds_float)

    return seconds_total

def get_time_as_text(seconds, seconds_fractured=True):
        hours, minutes, seconds = seconds_to_hms(seconds)
        if not seconds_fractured:
            seconds = int(np.floor(seconds))
        return "{}:{}:{}".format(hours, minutes, seconds)

class PlayerReader:
    def __init__(self):
        self.track_time_last = 0
        self.last_large_delta_time = 0
        self.large_delta_times_seconds = 0
        self.track_time_last_timestamp = 0
        self.retrieved_track_time = 0
        self.last_print_time = 0
        self.current_track_time = 0

        retrieve_thread = threading.Thread(target=self.retrieve_track_time_loop)
        main_thread = threading.Thread(target=self.get_time_seconds_loop)
        main_thread.daemon = True

        retrieve_thread.start()
        main_thread.start() 

    def print_time(self, seconds, full_seconds=True, seconds_fractured=True):
        if full_seconds and time.time() - self.last_print_time < 1:
            # wait one seconds before next print
            return

        self.last_print_time = time.time()
        print(get_time_as_text(seconds=seconds, seconds_fractured=seconds_fractured))

    def get_time_seconds_loop(self):
        while(True):
            # current_track_time = retrieved_track_time
            if self.retrieved_track_time > 0:
                # successfully read time from audio player, validate
                estimated_time = self.estimate_time()
                if self.validate_time(self.retrieved_track_time, estimated_time):
                    # track time is valid and is most likely the actual playback time of the track
                    self.track_time_last = self.retrieved_track_time
                    self.track_time_last_timestamp = time.time()
                    self.current_track_time = self.retrieved_track_time
                else:
                    # track time is not valid, use estimated playback time instead
                    self.current_track_time = estimated_time
            else:
                # track time could not be retrieved from audio player, estimate current playback time instead
                self.current_track_time = self.estimate_time()
        
            # self.print_time_seconds(current_track_time, False)

    def estimate_time(self):
        time_delta = time.time() - self.track_time_last_timestamp
        return self.track_time_last + time_delta

    def validate_time(self, track_time, estimated_time):
        delta_last_time = track_time - self.track_time_last
        # is current track_time later than last_track time? (only a valid check if track has not been skipped back)
        delta_greater_zero = delta_last_time > 0
        delta_small = abs(delta_last_time) < 5

        total = delta_greater_zero and delta_small

        if not total:
            # count how often the validation failed in a row
            self.large_delta_times_seconds += time.time() - self.last_large_delta_time
            self.last_large_delta_time = time.time()

            if self.large_delta_times_seconds > 10:
                # after many fails in a row it must be assumed the "invalid" track_time is actually the correct one
                self.large_delta_times_seconds = 0
                print(self.large_delta_times_seconds, "*"*50)
                return True
        else:
            # reset counter on any successful validation
            self.large_delta_times_seconds = 0
            self.last_large_delta_time = time.time()

        return total

    def retrieve_track_time_loop(self):
        while True:
            self.retrieved_track_time = get_time_seconds(time_a_bbox)

def main():
    player_reader = PlayerReader()
    while True:
        current_track_time = player_reader.current_track_time
        player_reader.print_time_seconds(current_track_time, False)

if __name__ == "__main__":
    main()