#!/usr/bin/env python3
"""Plot the live microphone signal(s) with matplotlib.

Matplotlib and NumPy have to be installed.

"""
import sys
def print_immediatly(message):
    print(message)
    sys.stdout.flush()

print_immediatly("Loading audio analyzer...")

import argparse
import queue
from matplotlib.animation import FuncAnimation
import matplotlib.pyplot as plt
import numpy as np
import sounddevice as sd
import librosa
import model
import socket


def int_or_str(text):
    """Helper function for argument parsing."""
    try:
        return int(text)
    except ValueError:
        return text

UDP_IP = "127.0.0.1"
UDP_PORT = 5005

parser = argparse.ArgumentParser(add_help=False)
parser.add_argument(
    '-l', '--list-devices', action='store_true',
    help='show list of audio devices and exit')
args, remaining = parser.parse_known_args()
if args.list_devices:
    print(sd.query_devices())
    parser.exit(0)
parser = argparse.ArgumentParser(
    description=__doc__,
    formatter_class=argparse.RawDescriptionHelpFormatter,
    parents=[parser])
parser.add_argument(
    '-c', '--channels', type=int, default=[1], nargs='*', metavar='CHANNEL',
    help='input channels to plot (default: the first)')
parser.add_argument(
    '-w', '--window', type=float, default=200, metavar='DURATION',
    help='visible time slot (default: %(default)s ms)')
parser.add_argument(
    '-i', '--interval', type=float, default=30,
    help='minimum time between plot updates (default: %(default)s ms)')
parser.add_argument(
    '-b', '--blocksize', type=int, help='block size (in samples)')
parser.add_argument(
    '-r', '--samplerate', type=float, help='sampling rate of audio device')
parser.add_argument(
    '-n', '--downsample', type=int, default=2, metavar='N',
    help='display every Nth sample (default: %(default)s)')
parser.add_argument(
    '-u', '--udp', action='store_false',
    help='Use udp to transfer data to application (default: %(default)s)')
args = parser.parse_args(remaining)
if any(c < 1 for c in args.channels):
    parser.error('argument CHANNEL: must be >= 1')
mapping = [c - 1 for c in args.channels]  # Channel numbers start with 1

q = queue.Queue()
show_plot = False

def audio_callback(indata, frames, time, status):
    """This is called (from a separate thread) for each audio block."""
    if status:
        print(status, file=sys.stderr)
    # Fancy indexing with mapping creates a (necessary!) copy:
    q.put(indata[::args.downsample, mapping])


def update_plot():
    """This is called by matplotlib for each plot update.

    Typically, audio callbacks happen more frequently than plot updates,
    therefore the queue tends to contain multiple blocks of audio data.

    """
    global plotdata
    global data_novelty_energy
    global data_novelty_spectral
    global data_zero_crossing
    global history
    while True:
        try:
            data = q.get_nowait()
            mel = get_mel(data)
            mel = mel[:,:2]
            mel = np.swapaxes(mel, 0, 1)
            mel = np.reshape(mel, (1, 2, 96))
            drums = model.get_output(mel)[0]
            volume = np.sum(mel)
            # log compression
            volume = np.log1p(10*volume)

            novelty_energy = get_novelty_energy(data)
            data_novelty_energy = novelty_energy

            zero_crossing = get_zero_crossing_rate(data)[0][0]
            # data_zero_crossing = zero_crossing

            shift = len(data)
            history = np.roll(history, -shift, axis=0)
            history[-shift:] = data
            novelty_spectral = get_novelty_spectral(history)
            # data_novelty_energy = novelty_spectral

            data = data_novelty_energy

            shift = len(data)

            novelty_energy_max = np.max(novelty_energy)
            novelty_spectral_max = np.max(novelty_spectral)
            novelty_spectral_max = np.max(novelty_spectral)

            message = "{},{},{},{},{},{},{}".format(volume, drums[0], drums[1], drums[2], novelty_energy_max, novelty_spectral_max, zero_crossing)
            if (args.udp):
                print("message:", message)
                sock.sendto(bytes(message, "utf-8"), (UDP_IP, UDP_PORT))
            else:
                print_immediatly(message)
        except queue.Empty:
            break
        plotdata = np.roll(plotdata, -shift, axis=0)
        plotdata[-shift:, :] = data
        # novelty_data = np.roll(novelty_data, -shift, axis=0)
        # novelty_data[-shift:, :] = novelty

def update_plot_anim(frame):
    update_plot()
    # for column, line in enumerate(lines):
    #     line.set_ydata(plotdata[:, column])
    # return lines

def get_mel(data):
    frame_length = len(data)
    hop_length = int(frame_length / 2)
    mel = librosa.feature.melspectrogram(y=data.astype(float).flatten(), n_mels=96, n_fft=frame_length, hop_length=hop_length, sr=args.samplerate)

    # print(frame_length, hop_length, mel.shape)
    return mel

def get_novelty_energy(data):
    # get root-mean-square energy
    hop_length = 64
    frame_length = len(data)
    data = data.reshape(data.shape[0])
    rmse = librosa.feature.rms(y=data, frame_length=frame_length, hop_length=hop_length).flatten()

    # perform logarithmic compression
    log_rmse = np.log1p(10*rmse)
    log_rmse_diff = np.zeros_like(log_rmse)
    log_rmse_diff[1:] = np.diff(log_rmse)
    log_energy_novelty = np.max([np.zeros_like(log_rmse_diff), log_rmse_diff], axis=0)

    return log_energy_novelty.reshape(log_energy_novelty.shape[0], 1)

def get_novelty_spectral(data):
    data = data.reshape(data.shape[0])
    spectral_novelty = librosa.onset.onset_strength(y=data)
    return spectral_novelty.reshape(spectral_novelty.shape[0], 1) / 10

def get_zero_crossing_rate(data):
    data = data.reshape(data.shape[0])
    zero_crossing = librosa.feature.zero_crossing_rate(data)
    return zero_crossing

try:
    devices = sd.query_devices()
    i = 0
    # auto
    for d in devices:
        print(i, d["name"])
        if "Stereomix" in d["name"]:
            device = i
            break
        i += 1

    # user selection
    for d in devices:
        print(i, d["name"])
        i += 1
    device = int(input("Select device by number: "))

    if args.samplerate is None:
        device_info = sd.query_devices(device, 'input')
        args.samplerate = device_info['default_samplerate']

    length = int(args.window * args.samplerate / (1000 * args.downsample))
    plotdata = np.zeros((length, len(args.channels)))
    novelty_data = np.zeros((length, len(args.channels)))
    history = np.zeros((1024 * 2, len(args.channels)))

    fig, ax = plt.subplots()
    lines = ax.plot(plotdata)
    if len(args.channels) > 1:
        ax.legend(['channel {}'.format(c) for c in args.channels],
                  loc='lower left', ncol=len(args.channels))
    ax.axis((0, len(plotdata), -1, 1))
    ax.set_yticks([0])
    ax.yaxis.grid(True)
    ax.tick_params(bottom=False, top=False, labelbottom=False,
                   right=False, left=False, labelleft=False)
    fig.tight_layout(pad=0)

    stream = sd.InputStream(
        device=device, channels=max(args.channels),
        samplerate=args.samplerate, callback=audio_callback)
    # ani = FuncAnimation(fig, update_plot_anim, interval=args.interval, blit=True)

    print_immediatly("Starting audio analyzer...")

    if args.udp:
        global sock
        sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM) # UDP

    with stream:
        while True:
            update_plot()

    # with stream:
        # plt.show()
except Exception as e:
    parser.exit(type(e).__name__ + ': ' + str(e))