import tensorflow
tf = tensorflow.compat.v1
from tensorflow.python.platform import gfile
import numpy as np
import datetime

def list_ops():
    ops = tf.get_default_graph().get_operations()
    ops = [op for op in ops if op.name.startswith("model")]
    return ops

def get_op_names():
    return [(op.name, tf.get_default_graph().get_tensor_by_name(op.name + ":0").shape) for op in list_ops()]

def print_op_names():
    for name in get_op_names():
        print(name)

GRAPH_PB_PATH = 'C:/Users/Marius/Files/Unity/auto-vj/python/VGG2[1024].pb'
sess = tf.Session()
print("load graph")
with gfile.FastGFile(GRAPH_PB_PATH,'rb') as f:
    graph_def = tf.GraphDef()
graph_def.ParseFromString(f.read())
sess.graph.as_default()
tf.import_graph_def(graph_def, name='')

tensor_input1 = sess.graph.get_tensor_by_name("model/Placeholder:0")
tensor_placeholder2 = sess.graph.get_tensor_by_name("model/Placeholder_2:0")
tensor_out = sess.graph.get_tensor_by_name("model/Sigmoid:0")

def get_output(input):
    # now = datetime.datetime.now()
    out = sess.run(tensor_out, feed_dict={tensor_input1 : input, tensor_placeholder2: 0})
    # now = datetime.datetime.now() - now
    # print(now)
    return out

if __name__ == "__main__":
    print_op_names()
    input = np.ones((1, 2, 96))
    print(get_output(input))

