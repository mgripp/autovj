import socket
import time
# import RPi.GPIO as GPIO

# GPIO.setwarnings(False)
# GPIO.setmode(GPIO.BOARD)
# GPIO.setup(8, GPIO.OUT, initial=GPIO.LOW)

UDP_IP = "0.0.0.0"
UDP_PORT = 1248

socket = socket.socket(socket.AF_INET, # Internet
    socket.SOCK_DGRAM) # UDP
socket.bind((UDP_IP, UDP_PORT))

def receive():
    while True:
        data, addr = socket.recvfrom(1024) # buffer size is 1024 bytes
        if(str(data).find("beat") > -1):
            # GPIO.output(8, GPIO.HIGH)
            print(time.time(), "received message:", data)
            time.sleep(.02)
            # GPIO.output(8, GPIO.LOW)

def main():
    receive()

if __name__ == "__main__":
    main()