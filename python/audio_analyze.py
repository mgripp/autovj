import librosa
import matplotlib.pyplot as plt
import numpy as np

"""
In order for librosa to be able to load mp3, ffmpeg has to be installed
(simply copy folder somewhere on hard drive):
https://ffmpeg.zeranoe.com/builds/

Also, ffmpeg bin directory needs to be added to PATH:
https://www.wikihow.com/Install-FFmpeg-on-Windows

Tutorials:
https://towardsdatascience.com/extract-features-of-music-75a3f9bc265d
https://librosa.github.io/librosa/tutorial.html

Mel-Spectogram:
https://towardsdatascience.com/getting-to-know-the-mel-spectrogram-31bca3e2d9d0

Librosa Feature Extraction:
https://librosa.github.io/librosa/feature.html
"""


def analyze_track(path):
    audio_date , sample_rate = librosa.load(path, sr=44100)

    tempo, beat_frames = librosa.beat.beat_track(y=audio_date, sr=sample_rate, units="frames")
    beat_times = librosa.frames_to_time(beat_frames, sr=sample_rate)

    spectogram, frequencies, frequency_times = get_frequencies(audio_date, sample_rate)

    results = AnalyzationResults(
        tempo=tempo,
        beat_times=beat_times,
        spectogram=spectogram,
        frequencies=frequencies,
        frequency_times=frequency_times,
    )

    return results

"""
https://stackoverflow.com/questions/53506970/how-can-i-get-the-specific-frequency-at-a-specific-timestamp-in-an-audio-file
"""
def get_frequencies(audio_date, sr):
    n_fft = 1024
    hop_length = 512

    spectogram = np.abs(librosa.core.stft(audio_date, n_fft=n_fft, hop_length=hop_length))
    frequencies = librosa.core.fft_frequencies(n_fft=n_fft)
    frequency_times = librosa.core.frames_to_time(spectogram[0], sr=sr, n_fft=n_fft, hop_length=hop_length)

    print('spectrogram size', spectogram.shape)

    fft_bin = 14
    time_idx = 1000

    print('freq (Hz)', frequencies[fft_bin])
    print('time (s)', frequency_times[time_idx])
    print('amplitude', spectogram[fft_bin, time_idx])

    print(frequency_times)

    return spectogram, frequencies, frequency_times


class AnalyzationResults:
    def __init__(
        self,
        tempo,
        beat_times,
        spectogram,
        frequencies,
        frequency_times,
    ):
        self.tempo = tempo
        self.beat_times = beat_times
        self.spectogram = spectogram
        self.frequencies = frequencies
        self.frequency_times = frequency_times
        self.last_track_time = 0

    def get_audio_data(self, track_time):
        interval_start = min(self.last_track_time, track_time)
        interval_end = max(self.last_track_time, track_time)
        self.last_track_time = track_time

        beat = self.has_beat_in_interval(interval_start, interval_end)
        amplitude = self.get_amplitude(interval_start, interval_end)

        return beat, amplitude

    def get_amplitude(self, interval_start, interval_end):
        amp_time = self.get_earliest_time_in_interval(self.frequency_times, interval_start, interval_end)
        fft_bin = 0
        amp_time_index = np.where(self.frequency_times == amp_time)[0]
        amplitude = self.spectogram[fft_bin, amp_time_index]

        return amplitude

    def get_earliest_time_in_interval(self, times, interval_start, interval_end):
        earliest_time = None
        for temp_time in times:
            if temp_time >= interval_start and temp_time < interval_end:
                # this time lies between the last time this function was called and the current time --> earliest time in interval!
                earliest_time = temp_time
                break
            
            if temp_time > interval_end:
                # time is beyond the interval, all following times from the array will be as well --> abort
                break
        
        return earliest_time

    def has_beat_in_interval(self, interval_start, interval_end):
        beat = False

        for beat_time in self.beat_times:
            if beat_time >= interval_start and beat_time < interval_end:
                # this beat time lies between the last time this function was called and the current time --> beat event!
                beat = True
                break
            
            if beat_time > interval_end:
                # beat_time is beyond the interval, all following beat_times from the array will be as well --> abort
                break

        return beat


def main():
    audio_path = './Klanglos - Acid Trip (Original Mix) [ERROR Records] (160kbit_Opus).mp3'
    results = analyze_track(audio_path)

    print("BPM:", results.tempo)
    

if __name__ == "__main__":
    main()