"""
https://musicinformationretrieval.com/novelty_functions.html
"""

import numpy, scipy, matplotlib.pyplot as plt, IPython.display as ipd
import librosa, librosa.display
# get signal data
x, sr = librosa.load('audio/melo.wav')

# get root-mean-square energy
spectral_novelty = librosa.onset.onset_strength(x, sr=sr)
print(spectral_novelty)
print(spectral_novelty.shape)
frames = numpy.arange(len(spectral_novelty))
t = librosa.frames_to_time(frames, sr=sr)

plt.figure(figsize=(15, 4))
plt.plot(t, spectral_novelty, 'r-')
plt.xlim(0, t.max())
plt.xlabel('Time (sec)')
plt.legend(('Spectral Novelty',))

plt.show()
