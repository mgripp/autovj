import numpy, scipy, matplotlib.pyplot as plt, IPython.display as ipd
import librosa, librosa.display

# get signal data
x, sr = librosa.load('audio/esm.wav')

# get root-mean-square energy
zero_crossing = librosa.feature.zero_crossing_rate(x)
frames = numpy.arange(len(zero_crossing))
t = librosa.frames_to_time(frames, sr=sr)

plt.figure(figsize=(15, 4))
plt.plot(zero_crossing[0], 'r-')
# plt.xlim(0, t.max())
plt.xlabel('Time (sec)')
plt.legend(('Zero crossing',))

plt.show()
