# Generate Activaton-Function in real-time --> Kapitel 4.3.3

from time import time
import numpy as np
import soundcard as sc
from struct import unpack
import matplotlib.pyplot as plt
import librosa

# Mic-Input = Loopback Monitor
print(sc.all_microphones(include_loopback=True)[0]) # [] --> check for sound sources

# model parameters
modelName =  './VGG2[1024].pb'
input_layer = 'model/Placeholder'
output_layer = 'model/Sigmoid'
patchSize = 2
labels = ['snare', 'hihat', 'kick']

# audio definitions
sampleRate = 16000
frameSize = 512
hopSize = 256
numberBands = 96

# audio buffers to record to for prediction and peak pipeline
bufferSize = 512
buffer = np.zeros(bufferSize, dtype='float32')

# for storing results
store = []

####################################
# Instantiate the algorithms ######
####################################

# audio InputBuffer
vimp = VectorInput(buffer)

# preprocessing for mel band computation
fc = FrameCutter(frameSize=frameSize, hopSize=hopSize, startFromZero=False) # zero centered

w = Windowing(normalized=False)

spec = Spectrum()

mel = MelBands(numberBands=numberBands, sampleRate=sampleRate,
               highFrequencyBound=sampleRate // 2,
               inputSize=frameSize,
               weighting='linear', normalize='unit_tri',
               warpingFormula='slaneyMel')

# Algorithms for logarithmic compression of mel-spectrograms
shift = UnaryOperator(shift=1, scale=10000)
comp = UnaryOperator(type='log10')

vtt = VectorRealToTensor(shape=[1, 1, patchSize, numberBands], lastPatchMode='discard')
ttp = TensorToPool(namespace=input_layer)
tfp = TensorflowPredict(graphFilename=modelName,
                        inputs=[input_layer],
                        outputs=[output_layer], isTrainingName='model/Placeholder_2')
ptt = PoolToTensor(namespace=output_layer)
ttv = TensorToVectorReal()

# pool to store predictions
pool = Pool()
pool2 = Pool()

# prediction pipeline
vimp.data      >> fc.signal
fc.frame       >>  w.frame
w.frame        >>  spec.frame
spec.spectrum  >>  mel.spectrum
mel.bands      >>  shift.array
shift.array    >>  comp.array
comp.array     >>  vtt.frame
vtt.tensor     >>  ttp.tensor
ttp.pool       >>  tfp.poolIn
tfp.poolOut    >>  ptt.pool
ptt.tensor     >>  ttv.tensor
ttv.frame      >>  (pool, output_layer)


# running the prediction
def predict(data):

    # update audio buffer
    buffer[:] = array(unpack('f' * bufferSize, data))

    # reset algorithms
    reset(vimp)

    # start prediction
    run(vimp)
    store.append(pool[output_layer][-1, :].T.tolist())
    print(pool[output_layer][-1, :].T.tolist())

    pool.clear()

################################
# running real-time audio ######
################################

with sc.all_microphones(include_loopback=True)[0].recorder(samplerate=sampleRate) as mic:

    start = time()
    Record = True
    pTime = []

    while Record:

        # get audio data
        data = mic.record(numframes=bufferSize).mean(axis=1)

        # start predictin only if audio is being streamed into the programm
        peak = np.mean(data)

        # peak != 0 --> anscheinend scheint der Null-Puffer, welcher in Kapitel 4.3.3 angesprochen wird, nicht
        # ausschließlich Nullen zu enthalten, da das if-statement weiter unten nicht zieht.
        # np.mean(data) ist auch im Falle eines Null-Puffers != 0
        # Vielleicht sind die Werte nicht Null sondern nur annähernd Null
        # oder der FramCutter schneidet nicht ausschließlich in einem Null-Bereich

        if peak != 0:

            # start the prediction
            start_time = time()
            predict(data)
            predictionTime = time() - start_time
            pTime.append(predictionTime)


        else:
            print(peak)

        # finish audio stream after 15 seconds, not pretty but for the moment okay
        exitTime = time()-start

        if exitTime > 15:
            Record = False


#################################
# getting results/plotting ######
#################################

# to store the individual predictions into lists for plotting
snare = []
kick = []
hihat = []

# get single predictions for each instrument
for x in range(len(store)):
    snare.append(store[x][0])
    hihat.append(store[x][1])
    kick.append(store[x][2])

# mean prediction Time
print("meanTime: ", np.mean(pTime))
print("totalTime: ", np.sum(pTime))

audio, sr = librosa.load('/home/laborinux/Musik/Drumgroove1.wav', sr=16000)
print("File_length ", librosa.get_duration(audio, sr=16000))

# how many predictions total
print("hh", len(hihat))
print("snare", len(snare))
print("kick", len(kick))

# to percentage
hihat = np.array(hihat) * 100
snare = np.array(snare) * 100
kick = np.array(kick) * 100

# plotting results
fig, (ax1, ax2, ax3) = plt.subplots(3, sharey=True, sharex=True)

plt.xticks(np.arange(1, len(snare)+2, 1))
plt.ylim(0, 100)
plt.yticks(np.arange(0, 120, 20))

fig.tight_layout(pad=3)

ax1.title.set_text('HiHat')
ax1.set_ylabel('%', rotation='horizontal', labelpad=10)
ax1.plot(hihat, color='black')

ax2.title.set_text('Snare')
ax2.set_ylabel('%', rotation='horizontal', labelpad=10)
ax2.plot(snare, color='black')

ax3.title.set_text('Kick')
ax3.set_ylabel('%', rotation='horizontal', labelpad=10)
ax3.set_xlabel('Anzahl der analysierten Puffer')
ax3.plot(kick, color='black')

plt.show()




