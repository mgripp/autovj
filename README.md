# AutoVJ

This projects targets a fully automated music visualization solution.

It takes an audio input signal from any input device (stereo mix, micorphone, ...) and plays videos from video/ to that input. Use mp4-files only.

## How to Run
To any added video file you can additionally create a reversed version named ''<name-of-original-file>REV.mp4'. The application will make use of that reversed file automatically for reverse playback effects.

You then have to add all video files exclusing REV-files to a playlist located under: video/default.m3u

To run the application, run the backend from python/audio_analyze_realtime.py, choose the desired audio input and hit play in the Unity Editor.

## Requirements
Unity Version: 2020.1.14f1
Python version: 3.6.8
Python packages: see python/requirements.txt
